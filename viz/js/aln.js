/* ------------------------------------------------------------------------------
# Copyright (c) 2017 Jeremy Wang
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# ------------------------------------------------------------------------------*/


/*
 * Object encapsulating data and display of fragment alignments
 */

function Alignments() {
  this.init = function(s) {
    // query	target	qstart	qend(exclusive)	tstart	tend	score	qdata	alndata	tdata
    // -- start and end are genomic coordinates, not fragment indices!
    this.data = s.split('\n');
    var i = 0;
    while(i < this.data.length) {
      if(this.data[i].length == 0 || this.data[i][0] == '#') {
        this.data.splice(i, 1);
        continue;
      }
      this.data[i] = this.parse(this.data[i]);
      if(this.data[i].qaln.length != this.data[i].taln.length || this.data[i].qaln.length != this.data[i].aln.length) {
        console.error("In alignment " + i + ", query, alignment, and target are not all the same length.");
      }
      i++;
    }
  }

  this.parse = function(aln) {
    var fields = aln.split('\t');
    return {
      qname: fields[0],
      tname: fields[1],
      qstart: parseInt(fields[2]),
      qend: parseInt(fields[3]), // exclusive
      tstart: parseInt(fields[4]),
      tend: parseInt(fields[5]), // exclusive
      score: parseFloat(fields[6]),
      qaln: fields[7].split(','),
      aln: fields[8].split(','),
      taln: fields[9].split(',')
    };
  }
  
  this.init.apply(this, arguments);
}

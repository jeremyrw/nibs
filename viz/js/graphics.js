/* ------------------------------------------------------------------------------
# Copyright (c) 2017 Jeremy Wang
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# ------------------------------------------------------------------------------*/

function Graphics() {
  this.draw_histogram = function(data, node) {
    var labels = [];
    /*
    if(!Array.isArray(data)) { //probably a typed array, convert to normal array
      data = Array.prototype.slice.call(data);
    }
    */
    for(var i = 0; i < data.length; i++) {
      if(i % 100 == 0)
        labels.push(i);
      else
        labels.push("");
    }
    var chart = new Chart(node, {
      type: 'line',
      data: {
        labels: labels,
        datasets: [{
          label: 'Digestion fragment size histogram',
          data: data
        }]
      },
      options: {
        scales: {
          yAxes: [{
            scaleLabel: {
              display: true,
              labelString: "# Fragments"
            },
            ticks: {
              beginAtZero:true
            }
          }],
          xAxes: [{
            scaleLabel: {
              display: true,
              labelString: "Fragment size (bp)"
            }
          }]
        }
      }
    });
  }
}

/*
 * Stack of coordinate-consistent Tracks
 */
function Stack() {
  /*
  * Genome display track
  * - fixed to a particular set of data
  * - can be triggered to update to a particular range
  */
  function Track() {
    this.init = function(label, data, node) {
      this.width = 800;
      this.height = 50;
      this.label_div = document.createElement("div");
      this.label_div.setAttribute("class", "row_label");
      this.label_div.appendChild(document.createTextNode(label));
      this.canvas_div = document.createElement("div");
      this.canvas_div.setAttribute("class", "row_content");
      this.canvas_div.setAttribute("style", "width: " + this.width + "px;");
      node.appendChild(this.label_div);
      node.appendChild(this.canvas_div);
      this.canvas = document.createElement("canvas");
      this.canvas.setAttribute("width", this.width);
      this.canvas.setAttribute("height", this.height);
      this.canvas.setAttribute("style", "width: " + this.width + "px; height: " + this.height + "px;");
      this.canvas.setAttribute("class", "track");
      this.canvas_div.appendChild(this.canvas);
      this.ctx = this.canvas.getContext("2d");
    }

    this.set_height = function(h) {
      this.height = h;
      this.canvas.setAttribute("height", this.height);
      this.canvas.setAttribute("style", "width: " + this.width + "px; height: " + this.height + "px;");
    }

    this.update = function(start, end) {
      this.start = start;
      this.end = end;
      this.scale = this.width / (this.end - this.start);
      this.ctx.clearRect(0, 0, this.width, this.height);
      this.draw();
    }

    this.init.apply(this, arguments);
  }

  function FrgTrack() {
    this.draw = function() {
      this.ctx.fillStyle = "gray";
      var tot = 0;
      for(var i = 0; i < this.fragments.data.length; i++) {
        begin = tot;
        tot += this.fragments.data[i];
        if(tot >= this.start) {
          var x0 = (begin - this.start) * this.scale;
          var x1 = (tot - this.start) * this.scale;
          // fillRect(x, y, width, height);
          this.ctx.fillRect(x0+1, 0, x1-x0-2, this.height);
        }
        if(tot > this.end) {
          break;
        }
      }
    }
    // basically, a call to super(), will inherit Track
    Track.apply(this, arguments);

    // will overwrite init() defined in Track
    this.init = function(label, data, node) {
      this.fragments = new Fragments(data);
    }

    this.init.apply(this, arguments);
  }

  function SVTrack() {
    this.draw = function() {
      for(var i = 0; i < this.svs.data.length; i++) {
        if(this.svs.data[i].tend >= this.start) {
          var x0 = (this.svs.data[i].tstart - this.start) * this.scale;
          var x1 = (this.svs.data[i].tend - this.start) * this.scale;

          if(this.svs.data[i].type == "del") this.ctx.fillStyle = "red";
          else if(this.svs.data[i].type == "ins") this.ctx.fillStyle = "green";
          else if(this.svs.data[i].type == "inv") this.ctx.fillStyle = "orange";

          // fillRect(x, y, width, height);
          this.ctx.fillRect(x0+1, 0, Math.max(2, x1-x0-2), this.height);

          if(this.svs.data[i].type == "ins") {
            this.ctx.font = "10px Courier";
            this.ctx.fillText(this.svs.data[i].size + "bp", x0+3, this.height/2-5);
          }
        }
        if(this.svs.data[i].tstart > this.end) {
          break;
        }
      }
    }
    // basically, a call to super(), will inherit Track
    Track.apply(this, arguments);

    // will overwrite init() defined in Track
    this.init = function(label, data, node) {
      this.svs = new SVs(data);
    }

    this.init.apply(this, arguments);
  }

  function AlnTrack() {
    this.draw = function() {
      /*
       * Assume:
       * 1. the reference coordinates are fixed
       * 2. the target (tname) IS the reference
       */

      // actually, we'll check to confirm the reference is correct
      //
      if(this.aln.tname.toLowerCase() != engine.ref.toLowerCase()) {
        console.error("Alignment target ('" + this.aln.tname + "') does not match the loaded reference ('" + engine.ref + "').");
      }

      var qlen = this.aln.qend - this.aln.qstart;
      var tlen = this.aln.tend - this.aln.tstart;
      var qx = this.aln.tstart - (qlen - tlen) / 2;
      var tx = this.aln.tstart;

      if(qx > this.end) return;
      for(var i = 0; i < this.aln.qaln.length; i++) {
        qbegin = qx;
        tbegin = tx
        if(this.aln.taln[i] != '-') {
          tx += parseFloat(this.aln.taln[i]);
        }
        if(this.aln.qaln[i] != '-') {
          qx += parseFloat(this.aln.qaln[i]);
        } else {
          continue;
        }
        if(qx >= this.start) {
          var qx0 = (qbegin - this.start) * this.scale + 1;
          var qx1 = (qx - this.start) * this.scale - 1;
          // fillRect(x, y, width, height);
          this.ctx.fillStyle = "blue";
          this.ctx.fillRect(qx0, this.height/2, qx1-qx0, this.height/2);

          if(this.aln.taln[i] != '-') {
            var tx0 = (tbegin - this.start) * this.scale + 1;
            var tx1 = (tx - this.start) * this.scale - 1;
            this.ctx.fillStyle = "#CCCCCC";
            this.ctx.beginPath();
            this.ctx.moveTo(tx0, 0);
            this.ctx.lineTo(tx1, 0);
            this.ctx.lineTo(qx1, this.height/2 - 2);
            this.ctx.lineTo(qx0, this.height/2 - 2);
            this.ctx.closePath();
            this.ctx.fill();
          }
        }
        if(qx > this.end) {
          break;
        }
      }
    }
    // basically, a call to super(), will inherit Track
    Track.apply(this, arguments);

    // will overwrite init() defined in Track
    this.init = function(label, data, node) {
      this.aln = data;
      this.set_height(100);
    }

    this.init.apply(this, arguments);
  }

  this.init = function(node) {
    this.track_root = document.createElement("div");
    this.axis_root = document.createElement("div");
    node.appendChild(this.track_root);
    node.appendChild(this.axis_root);
    this.start = 0;
    this.end = 1000;
    this.tracks = [];
    this.add_axis();
  }

  this.update = function(start, end) {
    if(start != null) {
      this.start = start;
      this.end = end;
    }
    for(var t = 0; t < this.tracks.length; t++) {
      this.tracks[t].update(this.start, this.end);
    }
  }

  function AxisTrack() {
    this.draw = function() {
      var height = 15; // text height is *approximately 1.5 x px size
      var min_tick_spacing = this.width / height / 2;
      var tick_spacing = 1;
      while(this.width / ((this.end - this.start) / tick_spacing) < min_tick_spacing) {
        // try 1, 5, 10, 50, 100, 500, etc...
        if((''+tick_spacing).indexOf('5') > -1) {
          tick_spacing *= 2;
        } else {
          tick_spacing *= 5;
        }
      }
      var first_tick = (parseInt(this.start / tick_spacing) + 1) * tick_spacing;
      var last_tick = parseInt(this.end / tick_spacing) * tick_spacing;
      for(var t = first_tick; t <= last_tick; t += tick_spacing) {
        this.ctx.save();
        this.ctx.fillStyle = "black";
        var tick_pos = (t - this.start) * this.scale;
        this.ctx.translate(tick_pos, 0);
        this.ctx.rotate(Math.PI * 0.5); // 90 degress, text extending down
        this.ctx.font = "10px Courier";
        this.ctx.strokeStyle = "black";
        this.ctx.beginPath();
        this.ctx.moveTo(0,0);
        this.ctx.lineTo(4,0);
        this.ctx.stroke();
        this.ctx.fillText(""+t, 5, height/4);
        this.ctx.restore();
      }
    }

    // basically, a call to super(), will inherit Track
    Track.apply(this, arguments);

    this.init = function() {};

    this.init.apply(this, arguments);
  }

  this.add_track = function(label, data, type) {
    var div;
    var t;
    if(type == "frg") {
      div = document.createElement("div");
      this.track_root.appendChild(div);
      t = new FrgTrack(label, data, div);
      this.tracks.push(t);
    } else if(type == "aln") {
      var alns = new Alignments(data);
      // all will be added, but only the LAST alignment track will be returned by add_track
      for(var i = 0; i < alns.data.length; i++) {
        div = document.createElement("div");
        this.track_root.appendChild(div);
        t = new AlnTrack(label + "|" + alns.data[i].qname, alns.data[i], div);
        this.tracks.push(t);
      }
    } else if(type == "sv") {
      div = document.createElement("div");
      this.track_root.appendChild(div);
      t = new SVTrack(label, data, div);
      this.tracks.push(t);
    } else {
      console.error("Unknown file/track type '" + type + "'.");
    }
    this.update();
    return t;
  }

  this.add_axis = function() {
    var div = document.createElement("div");
    this.axis_root.appendChild(div);
    var t = new AxisTrack("Ref Position", null, div);
    this.tracks.push(t);
    this.update();
  }

  this.init.apply(this, arguments);
}


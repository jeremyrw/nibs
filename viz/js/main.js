/* ------------------------------------------------------------------------------
# Copyright (c) 2017 Jeremy Wang
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# ------------------------------------------------------------------------------*/

"use strict";

function Engine() {
  this.ref = null;
  this.config = {};

  this.init = function() {
    get_json_file("config", (function(f, c) {
      this.config = c;
      this.load_refs();
    }).bind(this));
    this.stack = new Stack(document.getElementById("viz"));
  }

  this.load_refs = function() {
    var ref;
    var ref_select = document.getElementById("ref_select");
    ref_select.addEventListener("change", (function(e) {
      this.update_ref(e.target.value);
    }).bind(this), false)
    for(ref in this.config.refs) {
      var ref_option = document.createElement("option");
      ref_option.value = ref;
      ref_option.textContent = ref;
      ref_select.appendChild(ref_option);
    }
  }

  this.update_ref = function(ref) {
    this.ref = ref;
    console.log("Switching reference to " + ref);
    this.ref_root = document.getElementById("ref_info");
    get_server_file(this.config.refs[ref].frg_file, (function(f, content) {
      var track = this.new_file(f, content);
      //track.fragments.draw_hist(this.ref_root);
    }).bind(this));
    this.ref_root.innerHTML = "Reference: " + ref + "<br/>Size: " + this.config.refs[ref].bp + " bp";
  }

  this.update_range = function(s) {
    // s is in the format "100k - 1m"
    console.log("New range: '" + s + "'.");
    var parts = s.split('-');
    if(parts.length != 2) {
      console.error("Could not parse position range '" + s + "'.");
      return;
    }
    var start = parts[0];
    var end = parts[1];
    var smod = start.charAt(start.length-1);
    if(smod == "k" || smod == "K") {
      start = parseInt(start.substr(0, start.length-1)) * 1000;
    } else if(smod == "m" || smod == "M") {
      start = parseInt(start.substr(0, start.length-1)) * 1000000;
    } else {
      start = parseInt(start);
    }
    var emod = end.charAt(end.length-1);
    if(emod == "k" || emod == "K") {
      end = parseInt(end.substr(0, end.length-1)) * 1000;
    } else if(emod == "m" || emod == "M") {
      end = parseInt(end.substr(0, end.length-1)) * 1000000;
    } else {
      end = parseInt(end);
    }
    if(isNaN(start) || isNaN(end)) {
      console.error("Could not parse position range '" + s + "'.");
      return;
    }
    this.stack.update(start, end);
  }

  this.new_file = function(f, content) {
    var file_type = f.substring(f.lastIndexOf('.') + 1).toLowerCase();
    console.log(f + " has type '" + file_type + "'.");
    var file_name = f.substr(f.lastIndexOf('/') + 1, f.lastIndexOf('.') - f.lastIndexOf('/') - 1);
    return this.stack.add_track(file_name, content, file_type);
  }
}

var engine = new Engine();
document.addEventListener("DOMContentLoaded", function() {
  console.log("Loading engine...");
  engine.init();
  init_dragdrop();
}, false);


/*
 * Intialize drag/drop
 */

function init_dragdrop() {
  // drag/drop file handling
  var state = document.getElementById('status');
  var holder = document.getElementById('file_drop');

  if (typeof window.FileReader === 'undefined') {
    state.innerHTML = 'Your browser does not support local file reading';
  }

  holder.addEventListener('dragenter', function(e) {
    e.preventDefault();
    this.style.borderColor = '#009900';
    return false;
  }, false);
  holder.addEventListener('dragover', function(e) {
    e.preventDefault();
    return false;
  }, false);
  holder.addEventListener('dragleave', function(e) {
    e.preventDefault();
    this.style.borderColor = '#333333';
    return false;
  }, false);
  holder.addEventListener('drop', function (e) {
    this.style.borderColor = '#333333';
    e.preventDefault();

    var file = e.dataTransfer.files[0];

    console.log("File size: " + file.size);
    if(file.size > 100000000) {
      var doit = confirm("The file you are attempting to load is over 100Mb. This may cause your browser and computer to freeze. Are you sure?");
      if (!doit) {
        return false;
      }
    }

    get_local_file(file, engine.new_file.bind(engine));
    return false;
  }, false);
}

/* ------------------------------------------------------------------------------
# Copyright (c) 2017 Jeremy Wang
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# ------------------------------------------------------------------------------*/


/*
 * I/O Utilities
 */

function get_local_file(f, callback) {
  console.log("Loading local file '" + f.name + "'...");
  var reader = new FileReader();
  reader.onload = function (event) {
    console.log(f.name + " loaded.");
    callback(f.name, event.target.result);
  }
  reader.onerror = function (event) {
    console.error("Error reading local file '" + f + "'.");
  }
  reader.readAsText(f);
}

function get_server_file(f, callback) {
  console.log("Loading server file '" + f + "'...");
  var request = new XMLHttpRequest();

  var config_callback = function() {
    if(request.status != 200) {
      console.error("Request for '" + f + "' returned " + request.status);
      return;
    }
    console.log(f + " loaded.");
    callback(f, request.responseText);
  }

  request.addEventListener("load", config_callback);
  request.open("GET", f);
  request.send();
}

function get_json_file(f, callback) {
  get_server_file(f, function(f, content){
    callback(f, JSON.parse(content));
  });
}

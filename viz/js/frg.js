/* ------------------------------------------------------------------------------
# Copyright (c) 2017 Jeremy Wang
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# ------------------------------------------------------------------------------*/

/*
 * Object encapsulating data and display of simple fragment lists
 */

function Fragments() {
  this.init = function(s) {
    this.data = s.split('\n');
    for(var i = 0; i < this.data.length; i++) {
      this.data[i] = parseInt(this.data[i]);
    }

    // compute and render histogram
    this.compute_hist();
  }

  this.draw_hist = function(node) {
    this.graphics = new Graphics();
    var hist_canvas = document.createElement("canvas");
    hist_canvas.setAttribute("style", "width:400px; height:200px;");
    node.appendChild(hist_canvas);
    this.graphics.draw_histogram(this.hist, hist_canvas);
  }

  this.compute_hist = function() {
    var max = 0;
    for(var i = 0; i < this.data.length; i++) {
      if(this.data[i] > max)
        max = this.data[i];
    }
    var buffer = new ArrayBuffer(4 * (parseInt(max) + 1));
    this.hist = new Uint32Array(buffer); // automatically initialized to 0
    for(var i = 0; i < this.data.length; i++) {
      this.hist[this.data[i]]++;
    }
  }
  
  this.init.apply(this, arguments);
}

# ------------------------------------------------------------------------------
# Copyright (c) 2017 Jeremy Wang
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# ------------------------------------------------------------------------------

import sys

std_table = {}
digest_table = {}
order_table = {}

for line in sys.stdin:
  size = int(line[:line.index(' ')])

  std = line[line.index('[')+1:line.index(']')]
  std = float(std[:std.index(' ')])

  line = line[line.index(']')+1:]
  order = line[line.index('[')+1:line.index(']')]
  order = float(order[:order.index(' ')])

  line = line[line.index(']')+1:]
  digest = line[line.index('[')+1:line.index(']')]
  digest = float(digest[:digest.index(' ')])

  line = line[line.index(']')+1:]
  acc = float(line[line.index('(')+1:line.index(')')])

  if not std_table.has_key(size):
    std_table[size] = {}
    digest_table[size] = {}
    order_table[size] = {}

  if digest == 1 and order == 0:
    std_table[size][std] = acc
  if std == 1 and order == 0:
    digest_table[size][digest] = acc
  if std == 1 and digest == 1:
    order_table[size][order] = acc

std_vals = sorted(list(set(key for size in std_table.keys() for key in std_table[size].keys())))
digest_vals = sorted(list(set(key for size in digest_table.keys() for key in digest_table[size].keys())))
order_vals = sorted(list(set(key for size in order_table.keys() for key in order_table[size].keys())))

print std_table

print "STD:"
print "\t" + '\t'.join("%.2f" % v for v in std_vals)
for size in sorted(std_table.keys()):
  print "%i\t" % size + '\t'.join(("%.2f" % std_table[size][a] if std_table[size].has_key(a) else " ") for a in std_vals)

print
print "DIGEST:"
print "\t" + '\t'.join("%.2f" % v for v in digest_vals)
for size in sorted(digest_table.keys()):
  print "%i\t" % size + '\t'.join(("%.2f" % digest_table[size][a] if digest_table[size].has_key(a) else " ") for a in digest_vals)

print
print "ORDER:"
print "\t" + '\t'.join("%.2f" % v for v in order_vals)
for size in sorted(order_table.keys()):
  print "%i\t" % size + '\t'.join(("%.2f" % order_table[size][a] if order_table[size].has_key(a) else " ") for a in order_vals)

# ------------------------------------------------------------------------------
# Copyright (c) 2017 Jeremy Wang
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# ------------------------------------------------------------------------------

import random
import argparse
import simulate_nibs as sim
import sys
#sys.path.append("src")
import fasta
import numpy
from digest import digest
#import detect_sv as detect
import nibs_dtw as dtw

CHROMS = ['chr%i' % i for i in xrange(1,23)] + ['chrX']


def main(fa, size_std, frag_size, REs, outfile):
  seqs, names = fasta.read_fasta(fa)
  print names

  # count Ns at the beginning of each chrom, these aren't included in the digestion
  seq_ns = {}
  for name in names:
    n = 0
    while seqs[name][n] == 'N':
      n += 1
    seq_ns[name] = n

  res = REs.split(',')
  print "Restriction enzyme(s) to use:", res
  print "Ns:", seq_ns

  truth, truth_pos = digest(seqs, names, res, rate=1.0)

  chr_map = {}
  for t in xrange(len(truth_pos)):
    if not chr_map.has_key(truth_pos[t][0]):
      chr_map[truth_pos[t][0]] = t

  digestion = 1.0 # full digestion
  order_swap = 0.0 # perfect ordering

  size_results = []

  ref = random.choice([name for name in names if len(seqs[name]) - seq_ns[name] >= frag_size])

  start = random.randint(seq_ns[ref], len(seqs[ref]) - frag_size) if len(seqs[ref]) > frag_size else 0
  trial_seqs = {ref: seqs[ref][start:min(start + frag_size, len(seqs[ref]))]}
  actual_size = sum(len(s) for s in trial_seqs.values())

  # find true starting and ending pos/fragment for the trial subsequence
  start_fragment = chr_map[ref]
  pre_sum = 0
  print truth_pos[start_fragment], start
  while truth_pos[start_fragment][2] < start:
    pre_sum += truth[start_fragment]
    assert truth_pos[start_fragment][0] == ref
    #print ref, start, actual_size, start_fragment, truth_pos[start_fragment]
    start_fragment += 1
  end_fragment = start_fragment

  frg_sum = 0
  # for large fragments, this can continue to the end of the chromosome
  while truth_pos[end_fragment][0] == ref and truth_pos[end_fragment][2] < start + actual_size:
    frg_sum += truth[end_fragment]
    end_fragment += 1

  print "Simulating digestion from %s: %i-%i (of %i)" % (ref, start, min(start+frag_size, len(seqs[ref])), len(seqs[ref]))
  print "Fragments are %i-%i:" % (start_fragment, end_fragment), truth[start_fragment:start_fragment+10]
  print "Fragment sequence total before:", pre_sum
  print "Fragment sequence total within:", frg_sum


  # --------------------------------------------------
  # Simulate digestion, with various errors
  # --------------------------------------------------
  result = digest(trial_seqs, [ref], res, digestion)
  if result is None: # probably all Ns
    raise ValueError("Probably all Ns...")
  frags, positions = result
  if size_std > 0:
    # use size_std as a multiple of Laurent's function
    frags = sim.size_variance(frags, size_std, multiplier=True)
  if order_swap > 0:
    frags = sim.order_variance(frags, order_swap)

  print "  %i fragments" % len(frags)
  if len(frags) == 0: # this can happen if the entire sequence is Ns
    raise ValueError("No fragments, all Ns")

  # new aligner uses numpy arrays
  frags = numpy.array(frags, dtype='f4')
  truth = numpy.array(truth, dtype='f4')


  # --------------------------------------------------
  # Run DTW alignment
  # --------------------------------------------------

  # quick test of the DTW module
  #assert dtw.align(numpy.array([0,1,2,3,4,5], dtype='f4'), numpy.array([2,3,4], dtype='f4'), -1, -1) == [3.0, 0, 3, 2, 5, [0,0,0]], "DTW alignment doesn't seem to be working right"

  # ends positions are INCLUSIVE
  score, qstart, qend, tstart, tend, path = dtw.align(truth, frags, -1, -1)


  # --------------------------------------------------
  # Output
  # --------------------------------------------------

  if outfile is not None:
    write_alignment(frags, truth, qstart, qend, tstart, tend, score, path, outfile)

  print "Observed fragments %i-%i:" % (qstart, qend), frags[:10]
  print "Aligned to fragments %i-%i:" % (tstart, tend), truth[tstart:tstart+10]

  overlap = float(min(tend, end_fragment) - max(tstart, start_fragment) + 1) / (end_fragment - start_fragment + 1)
  if overlap < 0:
    overlap = 0
  size_results.append((frag_size, overlap))

  print "STDEV: %f, FRAGMENT SIZE: %i" % (size_std, frag_size)
  print "Results:"
  print size_results


def write_alignment(frags, truth, qstart, qend, tstart, tend, score, path, outfile):
  qaln = []
  q = qstart
  aln = []
  taln = []
  t = tstart
  for i in xrange(len(path)):
    if path[i] != 2: # not DEL
      qaln.append(frags[q])
      q += 1
    else:
      qaln.append('-')
    if path[i] != 1: # not INS
      taln.append(truth[t])
      t += 1
    else:
      taln.append('-')
    if path[i] == 0:
      aln.append('|')
    else:
      aln.append('*')
  fout = open(outfile, 'a')
  fout.write("%s\t%s\t%i\t%i\t%i\t%i\t%i\t%s\t%s\t%s\n" % ("simulated", "ref", frags[:qstart].sum(), frags[:qend].sum(), truth[:tstart].sum(), truth[:tend+1].sum(), score, ','.join(str(a) for a in qaln), ','.join(str(a) for a in aln), ','.join(str(a) for a in taln)))
  fout.close()


if __name__ == "__main__":
  parser = argparse.ArgumentParser("Simulate several size of input fragments to determine smallest feasible alignment")
  parser.add_argument("fasta", help="Genome FASTA")
  parser.add_argument("--std", help="Size deviation multiplier (from Laurent's standard)", type=float, default=1.0)
  parser.add_argument("--frag", help="Total fragment size", type=int, default=1000000)
  parser.add_argument("--out", help="Output tab-delimited alignments")
  parser.add_argument("REs", help="Restriction enzymes, comma separated")
  args = parser.parse_args()

  main(args.fasta, args.std, args.frag, args.REs, args.out)

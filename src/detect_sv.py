# ------------------------------------------------------------------------------
# Copyright (c) 2017 Jeremy Wang
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# ------------------------------------------------------------------------------

# ----------------------------------------------------------------
# Align and predict SVs from NIBS nanochannel fragments
#
# Began on or about 20141024
# ----------------------------------------------------------------

import matplotlib
matplotlib.use("Agg")
from matplotlib import pyplot as plt
import numpy
import math
import argparse
#import dp

def writeSVs(svs, out):
  fout = open(out, 'w')
  fout.write("sv_type,size,start,end")
  for sv in svs:
    fout.write("\n%s,%i,%i,%i" % (sv[0], sv[1], sv[2], sv[3]))
  fout.close()


'''
  Truth -->
F +----------------------
r |   ----> DEL
a |  |
g |  |   \
s |  |    \
  |  v     \
| |  INS  n/MISMATCH
| |
v |
  |
'''
# all Frags must be used, but we can use a subset of the Truth

INS = -1.0
DEL = -1.0

# set floor to 0 to identify good local alignments
def align(truth, frags, matrix_output, floor=None, ceil=None, local_threshold=None):
  matrix = numpy.zeros((len(frags), len(truth)))
  match_score = -math.fabs(frags[0] - truth[0]) / ((frags[0] + truth[0]) / 2.0)
  matrix[0,0] = max(match_score, INS, DEL)
  if floor is not None and matrix[0,0] < floor:
    matrix[0,0] = floor
  if ceil is not None and matrix[0,0] > ceil:
    matrix[0,0] = ceil
  for y in xrange(len(frags)):

    # old score:
    #match_score = -math.fabs(frags[y] - truth[0]) / ((frags[y] + truth[0]) / 2.0)

    # "baseline" is half the size of the smaller fragment
    # 1 if fragments are the same size
    # 0 if one fragment is 1.5x size of the other
    # -1 if one fragment is 2x size of the other (equivalent score to INDEL)
    baseline = max(min(frags[y], truth[0]) / 2, 1)
    match_score = (baseline - math.fabs(frags[y] - truth[0])) / baseline

    if y > 0:
      matrix[y,0] = matrix[y-1,0] + INS # DEL is not allowed here, we must account for all frags
      if floor is not None and matrix[y,0] < floor:
        matrix[y,0] = floor
      if ceil is not None and matrix[y,0] > ceil:
        matrix[y,0] = ceil
    for x in xrange(1, len(truth)):
      if frags[y] == 0 and truth[x] == 0: # I don't know if or why this happens
        match_score = 0
      else:
        baseline = max(min(frags[y], truth[x]) / 2, 1)
        match_score = (baseline - math.fabs(frags[y] - truth[x])) / baseline
      matrix[y,x] = max(
          (matrix[y-1,x-1] if y > 0 else 0) + match_score,
          (matrix[y-1,x] if y > 0 else 0) + INS,
          matrix[y,x-1] + DEL
      )
      if floor is not None and matrix[y,x] < floor:
        matrix[y,x] = floor
      if ceil is not None and matrix[y,x] > ceil:
        matrix[y,x] = ceil

  score = matrix[-1,:].max()
  #print "Alignment score:", score

  if matrix_output is not None:
    dp.drawMatrix(matrix_output, x=truth[:50], y=frags[:50], matrix=matrix[:50,:50])

  '''
  density = None
  '''
  density = numpy.zeros((min(1000, matrix.shape[0]),min(matrix.shape[1]*1000/matrix.shape[0]+1, matrix.shape[1])), dtype='u4')
  #scores = {}
  density_scalar = float(density.shape[0]) / matrix.shape[0]
  for i in xrange(matrix.shape[0]):
    if local_threshold is not None and max(matrix[i,:]) >= local_threshold:
      pts = numpy.where(matrix[i,:] == max(matrix[i,:]))[0]
      for p in pts:
        for x in xrange(-1,2):
          for y in xrange(-1,2):
            density[min(density.shape[0]-1, int(i*density_scalar)+y), min(density.shape[1]-1, int(p*density_scalar)+x)] += 1
  '''
      s = int(matrix[i,j])
      scores[s] = scores.get(s, 0) + 1
  scores = [(k,v) for k,v in scores.iteritems()]
  scores.sort(key = lambda a: a[0])
  print scores
  '''

  end = numpy.where(matrix == score)
  #print "Location:", end, "matrix is", matrix.shape

  # reverse reconstruct path
  y = end[0][-1] # get the LAST best score
  x = end[1][-1]
  path = []
  while y > 0 and x > 0:
    if matrix[y][x-1] > matrix[y-1][x] and matrix[y][x-1] > matrix[y-1][x-1]:
      x -= 1
      path.append(2) # DEL
    elif matrix[y-1][x] > matrix[y-1][x-1]:
      y -= 1
      path.append(1) # INS
    else: # matrix[y-1][x-1] is highest (or tied)
      y -= 1
      x -= 1
      path.append(0) # MATCH
  # Y must hit 0, X may not
  while y > 0:
    path.append(INS)
    y -= 1
  start = (x, y)
  # reverse
  path = path[::-1]

  return score, path, start, density

# DOESN'T WORK
def align_incomplete_digestion(truth, frags, matrix_output):
  matrix = numpy.zeros((len(frags), len(truth)))
  match_score = -math.fabs(frags[0] - truth[0]) / min(frags[0], truth[0])
  matrix[0,0] = max(match_score, INS, DEL)
  ins_total = numpy.zeros(len(truth)) # accumulate size of inserted fragments to add up for extra digestion
  del_total = 0
  if matrix[0,0] == match_score:
    ins_total[0] = frags[0]
    del_total = truth[0]
  for y in xrange(len(frags)):
    match_score = -math.fabs(frags[y] - truth[0]) / min(frags[y], truth[0])
    if y > 0:
      del_total = 0 # accumulate size of deleted fragments to add up for missed digestion
    if y > 0:
      matrix[y,0] = matrix[y-1,0] + INS # DEL is not allowed here, we must account for all frags
    for x in xrange(1, len(truth)):
      match_score = -math.fabs(frags[y] - truth[x]) / min(frags[y], truth[x])
      if ins_total[x] > 0:
        ins_total[x] += frags[y]
        ins_score = -math.fabs(ins_total[x] - truth[x]) / min(ins_total[x], truth[x]) # to include extra digestion events
      else:
        ins_score = INS
      if del_total > 0:
        del_total += truth[x]
        del_score = -math.fabs(frags[y] - del_total) / min(frags[y], del_total) # to include lost digestion events
      else:
        del_score = DEL
      matrix[y,x] = max(
          (matrix[y-1,x-1] if y > 0 else 0) + match_score,
          (matrix[y-1,x] if y > 0 else 0) + max(INS, ins_score),
          matrix[y,x-1] + max(DEL, del_score)
      )
      if matrix[y,x] == (matrix[y-1,x-1] if y > 0 else 0) + match_score:
        ins_total[x] = frags[y]
        del_total = truth[x]

  score = matrix[-1,:].max()
  print "Alignment score:", score

  dp.drawMatrix(matrix_output, x=truth[:50], y=frags[:50], matrix=matrix[:50,:50])

  end = numpy.where(matrix == score)
  print "Location:", end, "matrix is", matrix.shape

  # reverse reconstruct path
  y = end[0][-1] # get the LAST best score
  x = end[1][-1]
  path = []
  while y > 0 and x > 0:
    if matrix[y][x-1] > matrix[y-1][x] and matrix[y][x-1] > matrix[y-1][x-1]:
      x -= 1
      if matrix[y][x-1] - matrix[y][x] < 1:
        path.append(4) # match w/extra truth
      else:
        path.append(2) # DEL
    elif matrix[y-1][x] > matrix[y-1][x-1]:
      y -= 1
      if matrix[y-1][x] - matrix[y][x] < 1:
        path.append(3) # match w/extra frags
      else:
        path.append(1) # INS
    else: # matrix[y-1][x-1] is highest (or tied)
      y -= 1
      x -= 1
      path.append(0) # MATCH
  # Y must hit 0, X may not
  while y > 0:
    path.append(INS)
    y -= 1
  start = (x, y)
  # reverse
  path = path[::-1]

  # run-length compress
  '''
  rlc = []
  last = path[0]
  count = 0
  for p in path:
    if p != last:
      rlc.append("%i%s" % (count, "mid"[last]))
      last = p
      count = 1
    else:
      count += 1
  rlc.append("%i%s" % (count, "mid"[last]))
  print rlc
  '''

  return score, path, start


# will predict indels if they change the # of breakpoints or affect more than one fragment
# indels which simply change the length of a single fragment may not be detected

def predict_indels(path, start, truth, frags):
  x, y = start
  svs = [] # of the form ({INS,DEL}, size, ~start, ~end, score?)
  lastdiff = 0 # the different in size between the last matching pair (frags - truth)
  indel = 0 # total size of current indel fragments
  pos = 0 # position in truth
  for i in xrange(len(path)):
    p = path[i]
    if p == 0: # MATCH, may be followed by several 3s and 4s
      '''
      frag_tot = frags[y]
      true_tot = truth[x]
      while i + 1 < len(path) and path[i+1] in [3,4]:
        i += 1
        p = path[i]
        if p == 3:
          x += 1
          true_tot += truth[x]
        elif p == 4:
          y += 1
          frag_tot += frags[y]
      diff = frag_tot - true_tot
      '''
      diff = frags[y] - truth[x]
      if indel == 0: # MATCH seen previously, can still add small indel for size differences
        if lastdiff < 0 and diff < 0: # small DEL
          delete_size = -lastdiff + -diff
          svs.append(["DEL", delete_size, pos - delete_size, pos, None])
        if lastdiff > 0 and diff > 0: # small INS
          insert_size = lastdiff + diff
          svs.append(["INS", insert_size, pos, pos, None])
      if indel < 0: # DEL seen previously
        delete_size = -indel + (-lastdiff if lastdiff < 0 else 0) + (-diff if diff < 0 else 0)
        svs.append(["DEL", delete_size, pos - delete_size, pos, None])
      elif indel > 0: # INS seen previously
        insert_size = indel + (lastdiff if lastdiff > 0 else 0) + (diff if diff > 0 else 0)
        svs.append(["INS", insert_size, pos, pos, None])
      lastdiff = diff
      indel = 0
      pos += truth[x] #true_tot
      x += 1
      y += 1
    elif p == 1: # INS
      if indel < 0:
        indel = 0
        lastdiff = 0
      indel += frags[y]
      y += 1
    elif p == 2: # DEL
      if indel > 0:
        indel = 0
        lastdiff = 0
      indel -= truth[x]
      x += 1
  # and for the last one:
  if indel < 0: # DEL seen previously
    delete_size = -indel + (-lastdiff if lastdiff < 0 else 0)
    svs.append(["DEL", delete_size, pos - delete_size, pos, None])
  elif indel > 0: # INS seen previously
    insert_size = indel + (lastdiff if lastdiff > 0 else 0)
    svs.append(["INS", insert_size, pos, pos, None])
  return svs

# annihilate adjacent indels (allows for incomplete/extra digestion)
def adjust_path(path, frags, truth):
  i = 0
  x = 0
  y = 0
  while i < len(path):
    if i > 0 and (path[i-1] == 0 and path[i] == 1) and math.fabs(frags[x] + frags[x-1] - truth[y-1]) < math.fabs(frags[x-1] - truth[y-1]): # INS
      #print "path[-2:]", path[i-1:i+1], "frags:", frags[x-1:x+1], "popping frag", frags[x], "path", path[i]
      frags[x-1] += frags[x]
      frags.pop(x)
      path.pop(i)
    elif i > 0 and (path[i-1] == 1 and path[i] == 0) and math.fabs(frags[x] + frags[x-1] - truth[y]) < math.fabs(frags[x] - truth[y]): # INS
      #print "path[-2:]", path[i-1:i+1], "frags:", frags[x-1:x+1], "popping frag", frags[x], "path", path[i]
      frags[x-1] += frags[x]
      frags.pop(x)
      y += 1
      path[i-1] = 0
      path.pop(i)
    elif i > 0 and (path[i-1] == 0 and path[i] == 2) and math.fabs(truth[y] + truth[y-1] - frags[x-1]) < math.fabs(frags[x-1] - truth[y-1]): # DEL
      #print "path[-2:]", path[i-1:i+1], "truth:", truth[y-1:y+1], "popping truth", truth[y], "path", path[i]
      truth[y-1] += truth[y]
      truth.pop(y)
      path.pop(i)
    elif i > 0 and (path[i-1] == 2 and path[i] == 0) and math.fabs(truth[y] + truth[y-1] - frags[x]) < math.fabs(frags[x] - truth[y]): # DEL
      #print "path[-2:]", path[i-1:i+1], "truth:", truth[y-1:y+1], "popping truth", truth[y], "path", path[i]
      truth[y-1] += truth[y]
      truth.pop(y)
      x += 1
      path[i-1] = 0
      path.pop(i)
    else:
      if path[i] == 0:
        x += 1
        y += 1
      elif path[i] == 1:
        x += 1
      elif path[i] == 2:
        y += 1
      i += 1

def predict_inversions(path, start, rev_path, rev_start):
  invs = []
  return invs

if __name__ == "__main__":
  parser = argparse.ArgumentParser("Detect structural variants from observed nanochannel fragments")
  parser.add_argument("frags", help="Observed fragments sizes")
  parser.add_argument("truth", help="Expected (in silico) fragment sizes")
  parser.add_argument("out", help="Output file for list of SVs (CSV)")
  args = parser.parse_args()

  frags = numpy.array([int(line.strip()) for line in open(args.frags)])
  truth = numpy.array([int(line.strip()) for line in open(args.truth)])

  score, path, start, forward_density = align(truth, frags, None, floor=0, ceil=20, local_threshold=10)
  rev_score, rev_path, rev_start, rev_density = align(truth[::-1], frags, None, floor=0, ceil=20, local_threshold=10)

  # adjust path to reflect extra or missed digestion events
  path = list(path)
  frags = list(frags)
  truth = list(truth)
  adjust_path(path, frags, truth)

  svs = predict_indels(path, start, truth, frags)
  # we're assuming at this moment that the primary digestion is oriented forward on the positive strand

  #rev_path = adjust_path(rev_path, frags, truth[::-1])
  #svs.extend(predict_inversions(path, start, rev_path, rev_start))

  density = forward_density + numpy.fliplr(rev_density)
  plt.imshow(density, cmap=matplotlib.cm.gist_yarg)
  plt.savefig(args.frags[:args.frags.rindex('.')] + ".alignment.png")
  plt.clf()

  writeSVs(svs, args.out)

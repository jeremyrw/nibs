# ------------------------------------------------------------------------------
# Copyright (c) 2017 Jeremy Wang
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# ------------------------------------------------------------------------------

#import dtw
import h5py
import math
import argparse
import numpy as np
import Image, ImageDraw
import matplotlib
matplotlib.use("Agg")
from matplotlib import pyplot as plt
import matplotlib.gridspec as gridspec

COLORS = [(255, 100, 0), (0, 255, 0)] # orange, green
INS = -2
DEL = -2
NORM_WINDOW_SIZE = 100
VERT_OFFSET = 5

def get_neutral_deviation():
  input_fast5 = "data/UNC_MJ0127RV_cell13_nc101_run_2053_1_ch52_file96_strand.fast5"
  f = h5py.File(input_fast5, 'r')
  model_data = f["Analyses"]["Basecall_2D_000"]["BaseCalled_template"]["Model"]
  level_std_total = sum(a[2] for a in model_data) # total of level_std
  neutral_deviation = level_std_total / model_data.shape[0] # average std deviation of kmer levels
  print "Neutral deviation:", neutral_deviation
  return neutral_deviation

# http://www.rigtorp.se/2011/01/01/rolling-statistics-numpy.html
def rolling_window(a, window):
  shape = a.shape[:-1] + (a.shape[-1] - window + 1, window)
  strides = a.strides + (a.strides[-1],)
  return np.lib.stride_tricks.as_strided(a, shape=shape, strides=strides)

def normalize(trace, window_size=100):
  # normalize traces (by simple average)
  # alignments are VERY sensitive to this normalization
  # we'll try a rolling average
  # to use np.mean, dtype must be specified or it will overflow the 'f2' datatype

  if trace.shape[0] < window_size:
    tot = 0.0
    for a in trace:
      tot += a
    avg = tot / trace.shape[0]
    trace -= avg
    return

  avgs = np.mean(rolling_window(trace, window_size), axis=-1, dtype=float)
  assert avgs.shape[0] == trace.shape[0] - (window_size - 1)

  for i in xrange(trace.shape[0]):
    trace[i] -= avgs[min(max(0, i-window_size/2), avgs.shape[0]-1)]

def discretize(trace):
  dtrace = np.zeros(trace.shape[0], dtype='f4')
  for t in xrange(trace.shape[0]):
    dtrace[t] = int(trace[t] * 10) / 10.0
  return dtrace

def main(trace_npys, aln=None, monotonic=False, matrix=None, score_plot=None, limits=None, discrete=False):
  traces = [np.load(npy) for npy in trace_npys]

  if limits is not None:
    for t in xrange(len(traces)):
      traces[t] = traces[t][(limits[t][0] if limits[t][0] is not None else 0) : ((limits[t][1]+1) if limits[t][1] is not None else len(traces[t]))]

  for t in xrange(len(traces)):
    normalize(traces[t])
    if discrete:
      traces[t] = discretize(traces[t]) # returns a whole new np array with dtype 'i4'

  neutral_deviation = get_neutral_deviation()
  match_score_func = lambda a,b: (1 + (math.fabs(a - b) / neutral_deviation * -2))
  paths = []
  for t in xrange(1, len(traces)):
    if matrix is not None or score_plot is not None:
      matrix_out = matrix
      score, path, start, matrix = dtw.align(traces[t], traces[t-1], ins_score=INS, del_score=DEL, match_score_func=match_score_func, return_matrix=True)
      if len(matrix.shape) == 2: # full 2d matrix
        last_row = matrix[-1,:]
      elif len(matrix.shape) == 1: #only last row
        last_row = matrix
    else:
      score, path, start = dtw.align(traces[t], traces[t-1], ins_score=INS, del_score=DEL, match_score_func=match_score_func)
    print t, score, start, path
    paths.append((start, path))

  start, path = paths[0] # it is barely conceivable that there will be multiple alignments in the future

  '''
  print "this is NOT an alignment"
  print traces[t].shape[0], traces[t-1].shape[0], len(path)
  for i in xrange(traces[t-1].shape[0]):
    print traces[t-1][i], path[i], traces[t][start[0] + i]
  '''

  if aln is not None:
    fig = plt.figure()
    ax = fig.add_subplot(111)
    plot(traces, start, path, ax, monotonic=monotonic)
    plt.savefig(aln)

  if matrix_out is not None:
    score_matrix = get_score_matrix(matrix)
    plot_matrix(traces, score_matrix, start, path, last_row, len(traces[0]), matrix_out, scale=1)

    # output raw matrix to CSV
    '''
    fout = open(matrix_out[:matrix_out.rindex('.')] + ".csv", 'w')
    fout.write('\n'.join(','.join(str(a) for a in row) for row in matrix))
    fout.close()
    '''

  if score_plot is not None:
    fig = plt.figure()
    ax = fig.add_subplot(111)
    plot_score(last_row, ax, len(traces[0]))
    plt.savefig(score_plot)


def get_score_matrix(matrix):
  score_matrix = np.zeros((matrix.shape[0]-1, matrix.shape[1]-1), dtype=matrix.dtype)
  for i in xrange(1, matrix.shape[0]):
    for j in xrange(1, matrix.shape[1]):
      if matrix[i,j] > matrix[i-1,j] + INS and matrix[i,j] > matrix[i,j-1] + DEL:
        score_val = max(0.0, (matrix[i,j] - matrix[i-1,j-1] + 2) / 3.0)
      else:
        score_val = 0.0
      score_matrix[i-1,j-1] = score_val
  return score_matrix

def make_colormap():
  cdict = {'red':  ((0.0, 0.0, 0.0),
                     (1.0, 1.0, 1.0)),

           'green': ((0.0, 0.0, 0.0),
                     (1.0, 1.0, 1.0)),

           'blue':  ((0.0, 1.0, 1.0),
                     (1.0, 0.0, 0.0))
          }

  plt.register_cmap(name='by', data=cdict)

def plot_matrix(traces, matrix, start, path, last_row, qlen, out, scale=10):
  dpi = 100
  width = int((matrix.shape[1]/(dpi/scale)+1)) # * 1.25)
  height = int((matrix.shape[0]/(dpi/scale)+1) * 2) # * 2.5)
  fig = plt.figure(figsize=(width, height), dpi=dpi)

  #gs = gridspec.GridSpec(3, 2, width_ratios=[1,4], height_ratios=[2,2,1])
  gs = gridspec.GridSpec(3, 1, height_ratios=[3,4,1])
  ax_matrix = plt.subplot(gs[1]) # 3
  ax_aln = plt.subplot(gs[0], sharex=ax_matrix) # 1
  #ax_read_trace = plt.subplot(gs[2])
  ax_score = plt.subplot(gs[2], sharex=ax_matrix) # 5


  # alignment plot
  plot(traces, start, path, ax_aln, monotonic=True)
  ax_aln.set_xlim(0, matrix.shape[1])


  # trace plots
  '''
  ax_read_trace.plot(traces[0], xrange(traces[0].shape[0])) # plot vertically
  ax_read_trace.set_ylim(0, traces[0].shape[0])
  ax_read_trace.invert_xaxis()
  ax_read_trace.invert_yaxis()
  '''


  # matrix plot
  make_colormap()
  ax_matrix.imshow(matrix, cmap=plt.get_cmap("by"), interpolation="nearest")

  line = [start]
  for p in path:
    if p == 0: # match
      line.append((line[-1][0]+1, line[-1][1]+1))
    elif p == 1: # ins
      line.append((line[-1][0], line[-1][1]+1))
    elif p == 2: # del
      line.append((line[-1][0]+1, line[-1][1]))
  ax_matrix.plot([a[0] for a in line], [a[1] for a in line], color="#FFFFFF")

  ax_matrix.set_ylim(-0.5, matrix.shape[0]-0.5)
  ax_matrix.invert_yaxis()

  ax_matrix.set_xlim(-0.5, matrix.shape[1]-0.5)

  ax_matrix.set_xlabel("Simulated reference current trace")
  ax_matrix.set_ylabel("Observed read current trace")


  # score plot
  plot_score(last_row, ax_score, qlen)
  ax_score.set_xlim(0, matrix.shape[1])


  plt.savefig(out, bbox_inches="tight")


def plot_score(last_row, ax, qlen):
  ax.plot(last_row)
  ax.set_ylabel("DTW score")
  ax.set_xlabel("Ending position in simulated reference trace (start ~ end - %i)" % qlen)


def plot(traces, start, path, ax, monotonic=False):
  a_y = list(traces[0])
  b_y = list(traces[1])

  x_start = start[0]

  if not monotonic:
    a = 0
    b = x_start
    for p in path:
      if p == 1: # INS
        b_y.insert(b, 0)
      elif p == 2: # DEL
        a_y.insert(a, 0)
      a += 1
      b += 1
  else:
    a = 0
    b = x_start
    map = [None for i in xrange(len(a_y))] # map 'a' pos to 'b'
    for p in path:
      if p == 0: # MATCH
        map[a] = b
        a += 1
        b += 1
      elif p == 1: # INS
        a += 1
      elif p == 2: # DEL
        b += 1

  # offset a_x by the offset of the *middle* of the 'a's
  # sometimes the middle doesn't hit anything, so we have to be flexible
  a_x_offset = None
  a_mid = len(a_y)/2
  while a_x_offset is None:
    a_x_offset = map[a_mid]
    a_mid += 1

  if monotonic:
    a_x = [a_x_offset - (len(a_y)/2) + i for i in xrange(len(a_y))]
  else:
    a_x = [x_start + i for i in xrange(len(a_y))]
  b_x = [i for i in xrange(len(b_y))]

  ax2 = ax.twinx()

  ax2.plot([a_x[i] for i in xrange(len(a_x)) if a_y[i] != 0], [a_y[i] for i in xrange(len(a_y)) if a_y[i] != 0], color='#FF5500', label="Read")
  for i in xrange(len(a_y)):
    if a_y[i] != 0:
      ax2.scatter([a_x[i]], [a_y[i]], color="#FF5500", marker='o', edgecolor='white')
      if monotonic:
        if map[i] is not None:
          ax.plot([a_x[i], b_x[map[i]]], [a_y[i]+VERT_OFFSET, b_y[map[i]]+0.5], color="#CCCCCC", linestyle='-');
      else:
        if b_y[i+x_start] != 0:
          ax.plot([a_x[i], a_x[i]], [a_y[i]+VERT_OFFSET, b_y[i+x_start]+0.5], color="#CCCCCC", linestyle='-');
  ax.plot([b_x[i] for i in xrange(len(b_x)) if b_y[i] != 0], [b_y[i] for i in xrange(len(b_y)) if b_y[i] != 0], color='#00AA00', label="Reference")
  for i in xrange(len(b_y)):
    if b_y[i] != 0:
      ax.scatter([b_x[i]], [b_y[i]], color="#00AA00", marker='o', edgecolor='white')

  ax.set_ylabel("Simulated reference current trace")
  ax.yaxis.label.set_color("#00AA00")
  ax.set_ylim(-5, 10)
  ax2.set_ylabel("Observed current trace")
  ax2.set_ylim(-10, 5)
  ax2.yaxis.label.set_color("#FF5500")


if __name__ == "__main__":
  parser = argparse.ArgumentParser("Draw alignment between two current traces")
  parser.add_argument("trace0", help="Observed trace")
  parser.add_argument("trace1", help="Observed trace")
  parser.add_argument("--t0s", help="Start position in trace 0", type=int)
  parser.add_argument("--t0e", help="End position in trace 0", type=int)
  parser.add_argument("--t1s", help="Start position in trace 1", type=int)
  parser.add_argument("--t1e", help="End position in trace 1", type=int)
  parser.add_argument("--aln", help="Output aligned overlay plot (PNG)")
  parser.add_argument("--matrix", help="Output maxtrix figure (PNG)")
  parser.add_argument("--nonmonotonic", help="Draw signal alignments so that matches line up but each signal does NOT increase monotonically", default=False, action="store_true")
  parser.add_argument("--score", help="Output score plot (across reference) (PNG)")
  parser.add_argument("--discrete", help="Discretize current values (to integers)", default=False, action="store_true")
  args = parser.parse_args()
  main([args.trace0, args.trace1], args.aln, not args.nonmonotonic, args.matrix, args.score, limits=[(args.t0s, args.t0e), (args.t1s, args.t1e)], discrete=args.discrete)

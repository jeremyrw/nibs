# ------------------------------------------------------------------------------
# Copyright (c) 2017 Jeremy Wang
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# ------------------------------------------------------------------------------

# ----------------------------------------------------------------
# Simulate output of NIBS nanochannel fragment sizing
#
# Jeremy Wang - 20141024
#
# 1. Simulate DNA structural variation
# 2. Fragment DNA in silico
# 3. Add variance to observed fragment sizes
# 4. Add variance to order of observed fragments
#
# Ideas for Future Jeremy:
# - randomly drop fragments to simulate sticking in nanochannel
# - selectively move smaller fragments forward in the order
# ----------------------------------------------------------------

import numpy
import random
import argparse
import matplotlib
matplotlib.use("Agg")
from matplotlib import pyplot as plt
from digest import *

COMPL = {'A':'T', 'T':'A', 'C':'G', 'G':'C', 'N':'N', 'a':'t', 't':'a', 'c':'g', 'g':'c', 'n':'n'}

def read_svs(csv):
  data = [line.strip().split(',') for line in open(csv).read().strip().split('\n')]
  return [(d[0], d[1], int(d[2]), int(d[3])) for d in data]

def read_genome(fasta):
  seqs = {}
  names = []
  seq = ""
  for line in open(fasta):
    if line[0] == '>':
      if len(seq) > 0:
        names.append(name)
        seqs[name] = seq
        seq = ""
      name = line.strip()[1:]
    else:
      seq += line.strip()
  names.append(name)
  seqs[name] = seq
  return seqs, names

# chrom,{INS,DEL,INV},start,end[INS:size]
def sv(seqs, svs):
  for s in svs:
    try:
      chrom = seqs.keys()[int(s[0])]
      print "Using chrom '%s'." % chrom
    except:
      chrom = s[0]
    if not seqs.has_key(chrom):
      print "SV ref name '%s' not found." % chrom
      continue
    if s[1] not in ["INS", "DEL", "INV", "DUP"]:
      print "SV not supported: '%s'." % s[1]
      continue
    if s[2] >= len(seqs[chrom]) or (s[1] != "INS" and s[3] >= len(seqs[chrom])):
      print "Ref seq '%s' not long enough for %s %i:%i" % (s[0], s[1], s[2], s[3])
      continue
    if s[1] == "INS":
      seqs[chrom] = seqs[chrom][:s[2]] + 'N'*s[3] + seqs[chrom][s[2]:]
    if s[1] == "DUP":
      seqs[chrom] = seqs[chrom][:s[2]] + seqs[chrom][s[2]:s[3]+1] + seqs[chrom][s[2]:]
    if s[1] == "DEL":
      seqs[chrom] = seqs[chrom][:s[2]] + seqs[chrom][s[3]+1:]
    if s[1] == "INV":
      seqs[chrom] = seqs[chrom][:s[2]] + ''.join([COMPL[a] for a in seqs[chrom][s[2]:s[3]+1][::-1]]) + seqs[chrom][s[3]+1:]
  return seqs


# from Laurent:
# RSD=1.0992*(fragment size in bp)^-0.31287
def size_variance(frags, stdev=0, multiplier=False):
  for f in xrange(len(frags)):
    if multiplier:
      std = (1.0992 * (frags[f]**-0.31287)) * stdev
    else:
      std = stdev
    if std <= 0:
      a = frags[f]
    else:
      a = int(numpy.random.normal(frags[f], std * frags[f], 1)[0])
      while a < 1:
        a = int(numpy.random.normal(frags[f], std * frags[f], 1)[0])
    frags[f] = a
  return frags

def order_variance(frags, chance):
  for f in xrange(len(frags) - 1):
    if random.random() < chance:
      tmp = frags[f]
      frags[f] = frags[f+1]
      frags[f+1] = tmp
  return frags

def histogram(frags, out):
  n, bins, patches = plt.hist(frags, 50)
  plt.xlabel("Fragment size")
  plt.ylabel("Count")
  plt.title("Fragment size histogram")
  plt.grid(True)
  plt.savefig(out)

if __name__ == "__main__":
  parser = argparse.ArgumentParser("Simulate genome structural variation, digestion, and nanopore detection")
  parser.add_argument("fasta", help="Genome FASTA")
  parser.add_argument("REs", help="Restriction enzyme names, comma-separated")
  parser.add_argument("out", help="Output file for list of fragment sizes")
  parser.add_argument("--digest_completion", help="Rate of digestion completion", type=float, default=1.0)
  parser.add_argument("--size_std", help="Standard deviation in detected fragment size, as a fraction of total size", type=float, default=0)
  parser.add_argument("--order_swap", help="Chance of adjacent fragments swapping location", type=float, default=0)
  parser.add_argument("--sv", help="File containing list of structural variants (CSV)")
  parser.add_argument("--hist", help="Output a histogram on fragment sizes with given name")
  args = parser.parse_args()

  seqs, names = read_genome(args.fasta)
  if args.sv is not None:
    svs = read_svs(args.sv)
    seqs = sv(seqs, svs)
  frags, frag_pos = digest(seqs, names, args.REs.split(','), args.digest_completion)
  if args.size_std > 0:
    frags = size_variance(frags, args.size_std)
  if args.order_swap > 0:
    frags = order_variance(frags, args.order_swap)
  if args.hist is not None:
    histogram(frags, args.hist)

  # write output
  fout = open(args.out, 'w')
  fout.write('\n'.join([str(f) for f in frags]))
  fout.close()

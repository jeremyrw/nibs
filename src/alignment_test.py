# ------------------------------------------------------------------------------
# Copyright (c) 2017 Jeremy Wang
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# ------------------------------------------------------------------------------

import random
import argparse
import simulate_nibs as sim
import numpy
from digest import digest
#import detect_sv as detect
import nibs_dtw as dtw

def sim_align(seqs, names, res, truth, size_std=0, digestion=1, order_swap=0, multiplier=False):

  # simulate digestion, with various errors
  res = digest(seqs, names, res, digestion)
  if res is None: # probably all Ns
    raise ValueError("Probably all Ns...")
  frags, positions = res
  if size_std > 0:
    frags = sim.size_variance(frags, size_std, multiplier=multiplier)
  if order_swap > 0:
    frags = sim.order_variance(frags, order_swap)

  print "  %i fragments" % len(frags)
  if len(frags) == 0: # this can happen if the entire sequence is Ns
    raise ValueError("No fragments, all Ns")

  # align
  #score, path, start, density = detect.align(truth, frags, None)
  #truth_start, frag_start = start # (x,y) from matrix

  # new aligner uses numpy arrays
  frags = numpy.array(frags, dtype='f4')
  truth = numpy.array(truth, dtype='f4')

  # path is now a numpy array of type 'u1' (uint8_t)
  score, qstart, qend, tstart, tend, path = dtw.align(truth, frags, -1, -1)
  truth_start, frag_start = tstart, qstart

  path = list(path)

  end = truth_start + (len(path) - path.count(1)) # start + (all path except INS)

  print "  observed fragments (starting at %i):" % frag_start, frags[:10]
  print "  aligned to fragments (starting at %i):" % truth_start, truth[truth_start:min(truth_start+10, end)]

  return (truth_start, end) # predicted fragment position range


if __name__ == "__main__":
  parser = argparse.ArgumentParser("Simulate several size of input fragments to determine smallest feasible alignment")
  parser.add_argument("fasta", help="Genome FASTA")
  parser.add_argument("--std", help="Size deviation multiplier (from Laurent's standard)", type=float, default=1.0)
  parser.add_argument("--order", help="Chance of two fragment swapping places", type=float, default=0.0)
  parser.add_argument("--digest", help="Chance of digestion at each read cut site", type=float, default=1.0)
  parser.add_argument("--frag", help="Total fragment size", type=int, default=1000000)
  parser.add_argument("--trials", help="Number of random trials", type=int, default=10)
  parser.add_argument("REs", help="Restriction enzymes, comma separated")
  args = parser.parse_args()

  seqs, names = sim.read_genome(args.fasta)

  size_std = args.std
  frag_size = args.frag
  n_trials = args.trials

  # count Ns at the beginning of each chrom, these aren't included in the digestion
  seq_ns = {}
  for name in names:
    n = 0
    while seqs[name][n] == 'N':
      n += 1
    seq_ns[name] = n

  res = args.REs.split(',')
  print "Restriction enzyme(s) to use:", res

  truth, truth_pos = digest(seqs, names, res, 1)

  chr_map = {}
  for t in xrange(len(truth_pos)):
    if not chr_map.has_key(truth_pos[t][0]):
      chr_map[truth_pos[t][0]] = t

  digestion = args.digest
  order = args.order

  size_results = []
  for trial in xrange(n_trials):

    ref = random.choice([chrom for chrom in names if len(seqs[chrom]) - seq_ns[chrom] >= frag_size])

    start = random.randint(seq_ns[ref], len(seqs[ref]) - frag_size) if len(seqs[ref]) > frag_size else 0
    trial_seqs = {ref: seqs[ref][start:min(start + frag_size, len(seqs[ref]))]}
    print "%s: %i-%i (of %i)" % (ref, start, min(start+frag_size, len(seqs[ref])), len(seqs[ref]))
    actual_size = sum(len(s) for s in trial_seqs.values())

    # find true starting and ending pos/fragment for the trial subsequence
    start_fragment = chr_map[ref]
    while truth_pos[start_fragment][2] < start:
      assert truth_pos[start_fragment][0] == ref
      #print ref, start, actual_size, start_fragment, truth_pos[start_fragment]
      start_fragment += 1
    end_fragment = start_fragment
    # for large fragments, this can continue to the end of the chromosome
    while truth_pos[end_fragment][0] == ref and truth_pos[end_fragment][2] < start + actual_size:
      end_fragment += 1

    # multiplier: use size_std as a multiplier of Laurent's experimentally predicted RSD
    print "%ibp fragment, trial %i" % (frag_size, trial)

    try:
      est_start, est_end = sim_align(trial_seqs, [ref], res, truth, digestion=digestion, order_swap=order, size_std=size_std, multiplier=True)
    except ValueError as e:
      print e, "skipping."
      continue

    print "  true fragments:", truth[start_fragment:min(start_fragment+10, end_fragment)]
    overlap = float(min(est_end, end_fragment) - max(est_start, start_fragment) + 1) / (end_fragment - start_fragment + 1)
    if overlap < 0:
      overlap = 0
    size_results.append((frag_size, overlap))

  print "std: %f, size: %i" % (size_std, frag_size), size_results
  matches = len([s for s in size_results if s[1] > 0])
  print "%i [%f STD] [%f ORDER] [%f DIGEST]: %i/%i (%.2f)" % (frag_size, size_std, order, digestion, matches, len(size_results), float(matches)/len(size_results))

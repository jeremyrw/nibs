# ------------------------------------------------------------------------------
# Copyright (c) 2017 Jeremy Wang
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# ------------------------------------------------------------------------------

import matplotlib
matplotlib.use("Agg")
from matplotlib import pyplot as plt
import nibs_dtw as dtw
import argparse
import numpy
import numpy.random as rnd
import draw_dtw

WINDOW = 20
JUMP = 10

def main(frags, insilico, do_stats):
  frags = numpy.array([float(f.strip()) for f in open(frags)], dtype='f4')
  ref = numpy.array([float(a.strip()) for a in open(insilico)], dtype='f4')

  print "Observed fragments: %i" % frags.shape[0]
  print "Reference fragments: %i" % ref.shape[0]

  # do median/variance normalization
  frags -= numpy.mean(frags)
  frags /= numpy.std(frags)
  ref -= numpy.mean(ref)
  ref /= numpy.std(ref)

  if do_stats:
    score_distr = random_align(frags, ref, trials=1000)
    print "Random scores:"
    print score_distr[-51:]
    print "p < 0.05 ==", score_distr[-50]

  # try a sliding window of 100pts from the observed trace
  for i in xrange(0, frags.shape[0], JUMP):
    score, qstart, qend, tstart, tend, path = dtw.align(ref, frags[i:min(frags.shape[0], i+WINDOW)], -1, -1, 1) # I think stddev should be 1 by definition since we normalized it
    ref_start, frag_start = tstart, qstart
    end = ref_start + (len(path) - path.count(1)) # start + (all path except INS)

    print "query %i - %i: %.4f" % (i, i+WINDOW, score)
    #print len(path), path
    #print "  observed fragments (starting at %i):" % (i+frag_start), frags[i+frag_start:i+frag_start+10]
    #print "  aligned to fragments (starting at %i):" % ref_start, ref[ref_start:ref_start+10]

    if score > score_distr[-51]:
      print "------ p < 0.05 significant (but multiple testing...) ------"
      fig = plt.figure(figsize=(40,6), dpi=100)
      ax = fig.add_subplot(111)
      #draw_dtw.plot([frags[i:min(frags.shape[0], i+WINDOW)], ref[max(0, ref_start - 10): min(len(ref), end + 10)]], [frag_start, min(10, ref_start)], path, ax, monotonic=True)
      draw_dtw.plot([frags[i:min(frags.shape[0], i+WINDOW)], ref], [frag_start, ref_start], path, ax, monotonic=True)
      plt.savefig("block_%i_%i_alignment.png" % (i, i+WINDOW))

def random_align(frags, ref, trials=100):
  scores = []
  for t in xrange(trials):
    sample = rnd.choice(frags, WINDOW, replace=False)
    rnd.shuffle(sample) # in place
    score, qstart, qend, tstart, tend, path = dtw.align(ref, sample, -1, -1, 1)
    scores.append(score)
  scores.sort()
  return scores

if __name__ == "__main__":
  parser = argparse.ArgumentParser("Align observed fragments to in silico digestion")
  parser.add_argument("frags", help="List of fragment sizes")
  parser.add_argument("insilico", help="List of in silico digestion fragment sizes")
  parser.add_argument("--stats", help="Run a bunch of random trials to determine alignment significance (SLOW)", default=False, action="store_true")
  args = parser.parse_args()
  main(args.frags, args.insilico, args.stats)


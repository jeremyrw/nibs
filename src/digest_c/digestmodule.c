/* ------------------------------------------------------------------------------
# Copyright (c) 2017 Jeremy Wang
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# ------------------------------------------------------------------------------*/

#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include "digest.h"
#include "digest.c"
#include <Python.h>
#include <numpy/arrayobject.h>

static PyObject* digest_wrapper(PyObject * self, PyObject * args) {

  // Python signature:
  // sizes, locs = digest_c.digest(seq, motifs, rate, nlimit)

  PyObject *ps = PyTuple_GetItem(&args[0], 0); // actually a string (PyStringObject)
  char *seq = PyString_AsString(ps);
  size_t seq_len = PyString_Size(ps);

  // list of motifs (strings)
  PyObject *pl = PyTuple_GetItem(&args[0], 1); // actually a list (PyListObject)
  size_t n_motifs = PyList_Size(pl);
  char **motifs = malloc(sizeof (char*) * n_motifs);
  int i;
  for(i = 0; i < n_motifs; i++) {
    char *m = PyString_AsString(PyList_GetItem(pl, i));
    motifs[i] = malloc(strlen(m) + 1);
    strcpy(motifs[i], m);
  }

  float digest_rate = PyFloat_AsDouble(PyTuple_GetItem(&args[0], 2));
  int nlimit = PyInt_AsLong(PyTuple_GetItem(&args[0], 3));

  //printf("qlen %d, tlen %d, ins_score %d, del_score %d\n", qlen, tlen, ins_score, del_score);

  vec sizes, locs;
  kv_init(sizes);
  kv_init(locs);
  int retval = digest(seq, seq_len, motifs, n_motifs, digest_rate, nlimit, &sizes, &locs);

  if(retval != 0) {
    printf("Digest failed: exit code %i\n", retval);
    Py_RETURN_NONE;
  }

  int vec_len = kv_size(sizes);
  if(vec_len != kv_size(locs)) {
    printf("Fragment sizes and positions not the same length\n");
    Py_RETURN_NONE;
  }
  PyObject* sz = PyList_New(vec_len);
  PyObject* lc = PyList_New(vec_len);
  for(i = 0; i < vec_len; i++) {
    // because we're popping them off the end, we have to insert them in reverse order
    PyList_SetItem(sz, vec_len-1-i, (PyObject*)PyInt_FromLong((int)kv_pop(sizes)));
    PyList_SetItem(lc, vec_len-1-i, (PyObject*)PyInt_FromLong((int)kv_pop(locs)));
  }
  
  // clean up a bit
  kv_destroy(sizes);
  kv_destroy(locs);

  PyObject* ret = PyList_New(2);
  PyList_SetItem(ret, 0, sz);
  PyList_SetItem(ret, 1, lc);

  return ret;
}

static PyMethodDef Methods[] = {
  { "digest", digest_wrapper, METH_VARARGS, "REDigest" },
  { NULL, NULL, 0, NULL }
};

DL_EXPORT(void) initdigest_c(void) {
  Py_InitModule("digest_c", Methods);
}

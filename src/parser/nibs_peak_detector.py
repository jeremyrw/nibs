# ------------------------------------------------------------------------------
# Copyright (c) 2017 Jeremy Wang
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# ------------------------------------------------------------------------------

import math
import argparse
import os
import sys
import numpy as np
from scipy.optimize import leastsq
from scipy import signal
import time

# threshold each peak by maxima height (mean + n std), total width, and sum of intensity
ADJACENT_THRESHOLD = 100
WIDTH_THRESHOLD = 100
FREQUENCY_CUTOFF = 0.02
SD_THRESHOLD = 3
BASELINE_PCT = 99

def detect_peaks(fnames, peak_area_output, peak_edge_output=None, smoothed_output=False):
  t0 = time.time()
  print("Loading data.")
  data = []
  for fname in fnames:
    data.extend(open(fname).read().strip().split('\n'))
  print("%i points found." % len(data))
  data = [float(a) for a in data]
  a = np.array(data, dtype='f4')
  t1 = time.time()
  print("Done. %.2f seconds." % (t1-t0))

  print("Smoothing.")
  # build and apply the Butterworth filter
  N  = 3    # Filter order
  Wn = FREQUENCY_CUTOFF # Cutoff frequency
  B, A = signal.butter(N, Wn, output='ba')
  a = signal.filtfilt(B,A, a)
  t2 = time.time()
  print("Done. %.2f seconds." % (t2-t1))

  print "Computing baseline."
  # assume the lowest X% are the baseline
  limit = np.percentile(a, BASELINE_PCT)
  lower_pts = np.array([d for d in data if d <= limit], dtype='f4')
  baseline_order = 2
  line = np.polyfit(np.array(range(len(lower_pts))), lower_pts, 2) # quadratic fit
  baseline = np.array([sum([line[i] * (x**(baseline_order-i)) for i in range(baseline_order+1)]) for x in range(len(data))])
  lower_baseline = np.array([sum([line[i] * (x**(baseline_order-i)) for i in range(baseline_order+1)]) for x in range(len(data)) if data[x] <= limit])
  a = a - baseline
  print "Baseline: ", line[0], "x^2 + ", line[1], "x + ", line[2]
  threshold = np.std(lower_pts - lower_baseline) * SD_THRESHOLD # 3: 99.7%, 5: 99.99994% of baseline under this
  print "Threshold:", threshold
  if smoothed_output:
    print("Writing smoothed, background-subtracted data.")
    fout = open(smoothed_output, 'w')
    fout.write('\n'.join([str(pt) for pt in a]))
    fout.close()
  t3 = time.time()
  print "Done. %.2f seconds." % (t3-t2)

  print "Finding fragments."
  frags = get_maxima_fragments(a, threshold, WIDTH_THRESHOLD, np.mean(baseline))
  print len(frags), "fragments found."
  print "Average total intensity:", sum(f[2] for f in frags) / len(frags)
  t4 = time.time()
  print "Done. %.2f seconds." % (t4-t3)

  print "Fitting peaks."
  # modifies the peaks array to better estimate peak area, and returns peak shape equations
  peak_shapes = fit(frags, a)
  t5 = time.time()
  print "Done. %.2f seconds." % (t5-t4)

  print "Filtering peaks by area."
  frags = [f for f in frags if f[2] > threshold * WIDTH_THRESHOLD/2]
  print "%i fragments left." % len(frags)

  print("Writing output.")
  fout = open(peak_area_output, 'w')
  fout.write('\n'.join([str(f[2]) for f in frags]))
  fout.close()

  if peak_edge_output is not None:
    print("Writing peak coordinates.")
    fout = open(peak_edge_output, 'w')
    fout.write('\n'.join(["%i %i" % (f[0], f[1]) for f in frags]))
    fout.close()

  t5 = time.time()
  print("Done. %.2f seconds." % (t5-t4))


# detect all local extrema in data
def extrema(data, threshold):
  maxima = []
  minima = []
  prev = [(0, None), (0, None)]
  for d in range(1, len(data) - 1):
    if data[d] != prev[1][0]:
      if prev[1][0] > prev[0][0] and prev[1][0] > data[d] and prev[1][0] > threshold:
        maxima.append(prev[1][1])
      elif prev[1][0] < prev[0][0] and prev[1][0] < data[d]:
        minima.append(prev[1][1])
      prev[0] = prev[1]
      prev[1] = (data[d], d)
  return np.array(maxima), np.array(minima)


# detect peaks using minima - maxima - minima approach
def get_maxima_fragments(data, threshold, width_threshold, baseline):
  maxima, minima = extrema(data, threshold)
  print len(maxima), "maxima peaks"
  i = 0
  frags = []
  for m in maxima:
    while i < len(minima) and minima[i] < m:
      i += 1
    left = minima[i-1] if i > 0 else 1
    right = minima[i] if i < len(minima) else len(data)-1
    if right - left >= width_threshold:
      frags.append([left, right, sum(data[left: right+1]) - baseline * (right-left+1)])
  return frags


# estimate parameters and fit lognormal shape to peaks
def fit(peaks, data, verbose=False):
  margin = 1000 # margin around detected peak in which to integrate area
  start = 0
  p = 1
  peak_shapes = []
  while p <= len(peaks): # make sure we do that last peak
    if p >= len(peaks) or peaks[p][0] - peaks[p-1][1] > ADJACENT_THRESHOLD:

      estimated_peaks = []
      extra_params = []
      for i in xrange(start, p):
        peakdata = data[peaks[i][0]:peaks[i][1]+1]
        maximum = np.max(peakdata)

        # estimate parameters for (location, logmean, logsd, yscale)
        estimated_peaks.append((peaks[i][0], math.log((peaks[i][1]-peaks[i][0])*.25), maximum * 2.5))
        extra_params.append((peaks[i][0] + np.where(peakdata==maximum)[0][0], maximum))

      params, y_est = successive_fit(np.array(range(peaks[start][0], peaks[p-1][1]+1)), data[peaks[start][0] : peaks[p-1][1]+1], [(peaks[i][0]-peaks[start][0], peaks[i][1]-peaks[start][0]) for i in range(start, p)], estimated_peaks, extra=extra_params)

      num_params = len(estimated_peaks[0])
      for i in xrange(p-start):
        peaks[start+i][2] = sum([lognormal(x, params[i*num_params:(i+1)*num_params], extra_params[i][0], extra_params[i][1]) for x in range(peaks[start][0] - margin, peaks[p-1][1] + margin)])
        peak_shapes.append(params[i*num_params:(i+1)*num_params])
      start = p
    p += 1
  return peak_shapes


# probability distribution function for a lognormal distribution
def lognormal(x, params, xmode, maximum):
  location, sd, yscale = (params[0], params[1], params[2])
  mean = math.log(max(1, xmode - location))

  # just ln x, then do a normal distr
  x = np.log(x - location)
  x = np.nan_to_num(x) # replaces NaN with 0
  return yscale / (sd*np.sqrt(2*np.pi)) * np.exp(-(x - mean)**2/(2*sd**2))


# fit lognormal distributions to the given data from left to right
def successive_fit(x, y, peak_bounds, peak_params, extra=None):
  num_params = len(peak_params[0])

  y_curr = np.array(y) # full y, from which we'll subtract each peak as we go
  plsq = []

  for p in range(len(peak_params)):
    params = peak_params[p]
    peak_x = x[peak_bounds[p][0] : peak_bounds[p][1]]
    peak_y = y_curr[peak_bounds[p][0] : peak_bounds[p][1]]

    def res(params, y, x):
      y_fit = lognormal(x, params, extra[p][0], extra[p][1])
      err = y - y_fit
      return err

    plsq.append(leastsq(res, params, args = (peak_y, peak_x), maxfev=10000)[0])

    y_est = lognormal(x, plsq[p], extra[p][0], extra[p][1])
    y_curr -= y_est

  y_est = sum([lognormal(x, plsq[p], extra[p][0], extra[p][1]) for p in range(len(peak_params))])
  # flatten plsp to match output from ls_fit
  plsq = [a for b in plsq for a in b]
  return plsq, y_est


if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument("input", help="input may be a data file with one number per line or a file with a list of filenames, one per line")
  parser.add_argument("output", help="output file for ordered peak areas")
  parser.add_argument("--peaks", help="output file for peak beginning and end coordinates")
  parser.add_argument("--smoothed_output", help="output file for smoothed, background-subtracted input data")
  args = parser.parse_args()

  if not os.path.isfile(args.input):
    print("INPUT IS NOT A FILE")

  firstline = open(args.input, 'r').readline()
  try:
    num = float(firstline.strip())
    fnames = [args.input]
  except:
    fnames = open(args.input).read().strip().split('\n')

  detect_peaks(fnames, args.output, args.peaks, args.smoothed_output)

# ------------------------------------------------------------------------------
# Copyright (c) 2017 Jeremy Wang
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# ------------------------------------------------------------------------------

import math
import os
import sys
import numpy as np
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
from scipy.optimize import leastsq
from scipy import signal
from scipy import integrate
import time

# threshold each point by (mean + n std)
# threshold each peak by total width (not sum of intensity)

GROUP_THRESHOLD = 100
ADJACENT_THRESHOLD = 100
WIDTH_THRESHOLD = 100
FREQUENCY_CUTOFF = 0.02
SD_THRESHOLD = 3
BASELINE_PCT = 99

FIT = "lognormal"

def main(fnames, out_prefix):

  t0 = time.time()
  print "Loading data."
  data = []
  for fname in fnames:
    data.extend(open(fname).read().strip().split('\n'))
  if ' ' in data[0]: # multiple fiels, probably (time, val)
    for i in xrange(len(data)):
      data[i] = data[i].strip().split('   ')[1]
  print "points:", len(data)
  data = [float(a) for a in data]
  a = np.array(data, dtype='f4')

  '''
  fig = plt.figure(figsize=(20,5), dpi=100)
  ax = fig.gca() # get current axis
  ax.plot(a)
  plt.savefig("%s.data.png" % out_prefix)
  '''

  t1 = time.time()
  print "Done. %.2f seconds." % (t1-t0)
  #np.save(fname + ".npy", a)

  plot_peaks = range(0, len(data), 40000)
  #plot_peaks = range(0, 2000000, 10000)
  fit_peaks = False
  log_scale = False
  group = False

  if plot_peaks is not None:
    for i in xrange(len(plot_peaks)-1):
      plt.clf()
      plt.plot(a[plot_peaks[i]:min(plot_peaks[i+1], len(a))])
      plt.savefig("%s_pmt_raw_%i-%i.png" % (out_prefix, plot_peaks[i], plot_peaks[i+1]))

  print "Smoothing."
  #a = signal.medfilt(a, 201)
  #a = signal.wiener(a)
  #a = signal.savgol_filter(a, 201, 2) # window: 201 pts, polynomial order: 2

  # First, design the Butterworth filter
  N  = 3    # Filter order
  Wn = FREQUENCY_CUTOFF # Cutoff frequency
  B, A = signal.butter(N, Wn, output='ba')
  # Second, apply the filter
  a = signal.filtfilt(B,A, a)

  t2 = time.time()
  print "Done. %.2f seconds." % (t2-t1)

  '''
  if plot_peaks is not None:
    for i in xrange(len(plot_peaks)-1):
      plt.clf()
      plt.plot(a[plot_peaks[i]:min(plot_peaks[i+1], len(a))])
      plt.savefig("%s_pmt_smooth_%i-%i.png" % (out_prefix, plot_peaks[i], plot_peaks[i+1]))
  '''

  print "Computing baseline."
  # assume the lowest 90% are the baseline
  limit = np.percentile(a, BASELINE_PCT)
  lower_pts = np.array([d for d in data if d <= limit], dtype='f4')
  baseline_order = 2
  line = np.polyfit(np.array(range(len(lower_pts))), lower_pts, 2) # quadratic fit
  baseline = np.array([sum([line[i] * (x**(baseline_order-i)) for i in range(baseline_order+1)]) for x in range(len(data))])
  lower_baseline = np.array([sum([line[i] * (x**(baseline_order-i)) for i in range(baseline_order+1)]) for x in range(len(data)) if data[x] <= limit])

  '''
  if plot_peaks is not None:
    for i in xrange(len(plot_peaks)-1):
      plt.clf()
      subset = a[plot_peaks[i]:min(plot_peaks[i+1], len(a))]
      plt.plot(subset)
      plt.plot(baseline[plot_peaks[i]:min(plot_peaks[i+1], len(baseline))])
      plt.ylim([0, subset.max() + subset.max()*0.1])
      plt.savefig("%s_pmt_baseline_%i-%i.png" % (out_prefix, plot_peaks[i], plot_peaks[i+1]))
  '''

  a = a - baseline

  '''
  if plot_peaks is not None:
    for i in xrange(len(plot_peaks)-1):
      plt.clf()
      subset = a[plot_peaks[i]:min(plot_peaks[i+1], len(a))]
      plt.plot(subset)
      subsetandbaseline = subset + baseline[plot_peaks[i]:min(plot_peaks[i+1], len(a))]
      plt.ylim([0, subsetandbaseline.max() + subsetandbaseline.max()*0.1])
      plt.savefig("%s_pmt_nobaseline_%i-%i.png" % (out_prefix, plot_peaks[i], plot_peaks[i+1]))
  '''

  #print "Baseline:", np.mean(baseline), "+-", np.std(baseline)
  print "Baseline: ", line[0], "x^2 + ", line[1], "x + ", line[2]
  #threshold = np.mean(baseline) + np.std(baseline) * 2 # 3: 99.7%, 5: 99.99994% of baseline under this
  threshold = np.std(lower_pts - lower_baseline) * SD_THRESHOLD # 3: 99.7%, 5: 99.99994% of baseline under this
  print "Threshold:", threshold
  t3 = time.time()
  print "Done. %.2f seconds." % (t3-t2)

  print "Finding fragments."
  frags = get_maxima_fragments(a, threshold, WIDTH_THRESHOLD, np.mean(baseline))
  print len(frags), "fragments found."
  print "Average total intensity:", sum(f[2] for f in frags) / len(frags)
  t4 = time.time()
  print "Done. %.2f seconds." % (t4-t3)

  '''
  if plot_peaks is not None:
    for i in xrange(len(plot_peaks)-1):
      plt.clf()
      subset = a[plot_peaks[i]:min(plot_peaks[i+1], len(a))]
      plt.plot(subset)
      for f in xrange(len(frags)):
        if frags[f][1] > plot_peaks[i] and frags[f][0] < plot_peaks[i+1]:
          peakdata = a[frags[f][0]:frags[f][1]+1]
          maximum = np.max(peakdata)
          xmode = frags[f][0] + np.where(peakdata==maximum)[0][0]
          plt.plot([xmode-plot_peaks[i],xmode-plot_peaks[i]+1], [0,maximum]) # vertical line (height)
          plt.plot([frags[f][0]-plot_peaks[i], frags[f][1]-plot_peaks[i]], [maximum/2,maximum/2])
      plt.savefig("%s_pmt_extrema_%i-%i.png" % (out_prefix, plot_peaks[i], plot_peaks[i+1]))
  '''

  print "Fitting peaks."
  # modifies the peaks array to better estimate peak area, and returns peak shape equations
  peak_shapes = fit(frags, a)
  t5 = time.time()
  print "Done. %.2f seconds." % (t5-t4)

  print "Filtering peaks by area."
  f = 0
  while f < len(frags):
    if frags[f][2] < threshold * WIDTH_THRESHOLD/2:
      frags.pop(f)
      peak_shapes.pop(f)
    else:
      print frags[f]
      f += 1
  print "Top 10 fragments:", sorted(frags, key = lambda f: f[2]*-1)[:10]

  print "Plotting data w/peaks."
  if plot_peaks is not None:
    for i in xrange(len(plot_peaks)-1):
      plt.clf()
      plt.plot(a[plot_peaks[i]:min(plot_peaks[i+1], len(a))])
      #plt.savefig("%s_pmt_data_%i-%i.png" % (out_prefix, plot_peaks[i], plot_peaks[i+1]))

      # curve-fit peaks
      boundary = 5000
      for f in xrange(len(peak_shapes)):
        if frags[f][1] > plot_peaks[i] and frags[f][0] < plot_peaks[i+1]:
          peakdata = a[frags[f][0]:frags[f][1]+1]
          maximum = np.max(peakdata)
          xmode = frags[f][0] + np.where(peakdata==maximum)[0][0]
          plt.plot([pdf(FIT, j, peak_shapes[f], [xmode, maximum]) for j in xrange(plot_peaks[i], plot_peaks[i+1])])

      plt.savefig("%s_pmt_peaks_%i-%i.png" % (out_prefix, plot_peaks[i], plot_peaks[i+1]))

  t6 = time.time()
  print "Done. %.2f seconds." % (t6-t5)

  print("Writing area output.")
  fout = open(out_prefix + ".area.txt", 'w')
  fout.write("start,end,area\n")
  fout.write('\n'.join([str(f[0]) + ',' + str(f[1]) + ',' + str(f[2]) for f in frags]))
  fout.close()

  print "Computing and plotting fragment size histogram."
  bin_size = 0.05
  hist = [0 for i in xrange(int(max(f[2] for f in frags)/bin_size) + 1)]
  for f in frags:
    hist[int(f[2] / bin_size)] += 1
  print "Plotting fragment sizes."
  x = np.array(range(len(hist)))
  y = np.array(hist, dtype='u4')
  if fit_peaks:
    # for peak fitting, get rid of everything under 5
    y = y / np.max(y) # normalize to 0-1, this is necessary to fit properly
    multifit(x, y, [(a/20, 1, 2) for a in [125, 564, 2027, 2322, 4361, 6557, 9416, 23130]]) # /20 just makes it jive with the intensities we see at this sampling frequency
  if log_scale:
    for h in xrange(len(hist)):
      hist[h] = math.log(hist[h], 2) if hist[h] > 0 else 0
  fig = plt.figure()
  ax = fig.add_subplot(111)
  ax.set_ylabel("# fragments")
  ax.set_xlabel("Total fragment intensity")
  ax.plot(hist)
  ax.set_xlim(0, len(hist))
  if log_scale:
    ax.yaxis.set_ticklabels([(2**i) for i in xrange(0,15,2)])
  plt.savefig("%s_fragment_sizes.png" % out_prefix)
  t7 = time.time()
  print "Done. %.2f seconds." % (t7-t6)

  if group:
    print "Grouping fragments."
    groups = []
    group = []
    last = None
    for f in frags:
      if last == None or f[0] - last < GROUP_THRESHOLD:
        group.append(f)
      else:
        if len(group) > 0:
          groups.append(group)
          group = [f]
      last = f[1]
    group_lens = [len(g) for g in groups]
    print "Groups:", group_lens
    print "%i groups." % len(groups)
    print "Distribution:", [(c, group_lens.count(c)) for c in xrange(max(group_lens))]
    for g in groups:
      print [a[2] for a in g]
    plt.clf()
    for g in groups:
      if len(g) > 2:
        plt.plot([a[2] for a in g if a[2] < 100])
    plt.savefig("%s_peak_cluster_correlation.png" % out_prefix)
    t8 = time.time()
    print "Done. %.2f seconds." % (t8-t7)


def fit(peaks, data, verbose=False):
  margin = 1000 # margin around detected peak in which to integrate area
  start = 0
  p = 1
  peak_shapes = []
  while p <= len(peaks): # make sure we do that last peak
    if p >= len(peaks) or peaks[p][0] - peaks[p-1][1] > ADJACENT_THRESHOLD:

      estimated_peaks = []
      extra_params = []
      for i in xrange(start, p):
        peakdata = data[peaks[i][0]:peaks[i][1]+1]
        maximum = np.max(peakdata)

      # levy
      # rate of decay too shallow
        if FIT == "levy":
          # to start: location -> start of peak, mult -> maximum height, scale -> automatically computed
          estimated_peaks.append((peaks[i][0], maximum))
          extra_params.append((peaks[i][0] + np.where(peakdata==maximum)[0][0], maximum))

      # log normal
      # in testing
        if FIT == "lognormal":
          # (location, logmean, yscale)
          #estimated_peaks.append((peaks[i][0], math.log(peaks[i][0]), math.log((peaks[i][1]-peaks[i][0])*.25), maximum * 2.5))
          estimated_peaks.append((peaks[i][0], math.log((peaks[i][1]-peaks[i][0])*.25), maximum * 2.5))
          extra_params.append((peaks[i][0] + np.where(peakdata==maximum)[0][0], maximum))

      # normal (with mean and maximum fixed)
      # no long tail
        if FIT == "normal" or FIT == "fixed_normal":
          # (sd)
          # set std to 25% of estimated peak width
          estimated_peaks.append((np.sqrt(0.25 * (peaks[i][1] - peaks[i][0])), maximum))
          extra_params.append((peaks[i][0] + np.where(peakdata==maximum)[0][0],))

      # normal with exponential tail (with mean and maximum fixed)
      # need different exponential decay function
        if FIT == "normal_exponential":
          # 1: exponential decay rate
          estimated_peaks.append((np.sqrt(0.25 * (peaks[i][1] - peaks[i][0])), maximum, 1, maximum))
          extra_params.append((peaks[i][0] + np.where(peakdata==maximum)[0][0],))

      # gamma
      # can't get a fit (math problem)
        if FIT == "gamma":
          estimated_peaks.append((peaks[i][0], 0.5))
          extra_params.append((peaks[i][0] + np.where(peakdata==maximum)[0][0], maximum))

      if verbose:
        print "est peaks:", estimated_peaks
        print "xmode, scale:", extra_params
      #params, y_est = ls_fit(np.array(range(peaks[start][0], peaks[p-1][1]+1)), data[peaks[start][0] : peaks[p-1][1]+1], estimated_peaks, fit=FIT, extra=extra_params)
      params, y_est = successive_fit(np.array(range(peaks[start][0], peaks[p-1][1]+1)), data[peaks[start][0] : peaks[p-1][1]+1], [(peaks[i][0]-peaks[start][0], peaks[i][1]-peaks[start][0]) for i in range(start, p)], estimated_peaks, fit=FIT, extra=extra_params)


      if verbose:
        print "lsq params:", start, p-1, params
      num_params = len(estimated_peaks[0])
      for i in xrange(p-start):
        peaks[start+i][2] = sum([pdf(FIT, x, params[i*num_params:(i+1)*num_params], extra_params[i]) for x in range(peaks[start][0] - margin, peaks[p-1][1] + margin)])
        peak_shapes.append(params[i*num_params:(i+1)*num_params])
      start = p
    p += 1
  return peak_shapes

def extrema(data, threshold):
  maxima = []
  minima = []
  prev = [(0, None), (0, None)]
  for d in xrange(1, len(data) - 1):
    if data[d] != prev[1][0]:
      if prev[1][0] > prev[0][0] and prev[1][0] > data[d] and prev[1][0] > threshold:
        maxima.append(prev[1][1])
      elif prev[1][0] < prev[0][0] and prev[1][0] < data[d]:
        minima.append(prev[1][1])
      prev[0] = prev[1]
      prev[1] = (data[d], d)
  return np.array(maxima), np.array(minima)

def get_maxima_fragments(data, threshold, width_threshold, baseline):
  maxima, minima = extrema(data, threshold)
  #maxima = signal.argrelmax(data)[0]
  #minima = signal.argrelmin(data)[0]
  print len(maxima), "maxima peaks"
  i = 0
  frags = []
  for m in maxima:
    while i < len(minima) and minima[i] < m:
      i += 1
    left = minima[i-1] if i > 0 else 1
    right = minima[i] if i < len(minima) else len(data)-1
    if right - left >= width_threshold:
      frags.append([left, right, sum(data[left: right+1]) - baseline * (right-left+1)])
  return frags

# probability density function
def pdf(fit, x, params, extra_params):
  # levy (long tail)
  if fit == "levy":
    pdf = levy(x, params, extra_params[0], extra_params[1])
  # lognormal (long tail)
  elif fit == "lognormal":
    pdf = lognormal(x, params, extra_params[0], extra_params[1])
  elif fit == "fixed_normal":
    pdf = fixed_normal(x, params, extra_params[0])
  elif fit == "normal":
    pdf = normal(x, params, extra_params[0], extra_params[1])
  elif fit == "gamma":
    pdf = gamma(x, params, extra_params[0], extra_params[1])
  elif fit == "normal_exponential":
    pdf = fixed_normal(x, params[:2], extra_params[0]) + exp_tail(x, params[2:], extra_params[0])
  return np.array(pdf)

def ls_fit(x, y, expected_peaks, fit="normal", extra=None):
  num_params = len(expected_peaks[0])

  # solving
  params = [param for peak in expected_peaks for param in peak] # flatten

  def res(params, y, x):
    y_fit = sum([pdf(fit, x, params[p:p+num_params], extra[int(p/num_params)]) for p in xrange(0, len(params), num_params)])
    err = y - y_fit
    return err

  plsq = leastsq(res, params, args = (y, x), maxfev=10000)
  #plsq = simple_opt(res, params, args = (y, x))

  y_est = sum([pdf(fit, x, plsq[0][p:p+num_params], extra[int(p/num_params)]) for p in xrange(0, len(plsq[0]), num_params)])

  return plsq[0], y_est

def successive_fit(x, y, peak_bounds, peak_params, fit="normal", extra=None):
  num_params = len(peak_params[0])

  y_curr = np.array(y) # full y, from which we'll subtract each peak as we go
  plsq = []

  for p in range(len(peak_params)):
    params = peak_params[p]
    peak_x = x[peak_bounds[p][0] : peak_bounds[p][1]]
    peak_y = y_curr[peak_bounds[p][0] : peak_bounds[p][1]]

    def res(params, y, x):
      y_fit = pdf(fit, x, params, extra[p])
      err = y - y_fit
      return err

    plsq.append(leastsq(res, params, args = (peak_y, peak_x), maxfev=10000)[0])

    y_est = pdf(fit, x, plsq[p], extra[p])
    y_curr -= y_est

  y_est = sum([pdf(fit, x, plsq[p], extra[p]) for p in range(len(peak_params))])
  # flatten plsp to match output from ls_fit
  plsq = [a for b in plsq for a in b]
  return plsq, y_est

def fixed_normal(x, params, xmode):
  sd, mult = (params[0], params[1])
  return mult / (sd*np.sqrt(2*np.pi)) * np.exp(-(x - xmode)**2/(2*sd**2))

def exp_tail(x, params, xmode):
  rate, mult = (params[0], params[1])
  return mult * rate * np.exp(-rate*x)

def normal(x, params, xmode, maximum):
  mean, sd, mult = (params[0], params[1], params[2])
  return mult / (sd*np.sqrt(2*np.pi)) * np.exp(-(x - mean)**2/(2*sd**2))

def lognormal(x, params, xmode, maximum):
  #location, mean, sd, yscale = (params[0], params[1], params[2], params[3])
  location, sd, yscale = (params[0], params[1], params[2])
  mean = math.log(max(1, xmode - location))

  # just ln x, then do a normal distr
  x = np.log(x - location)
  x = np.nan_to_num(x) # replaces NaN with 0
  return yscale / (sd*np.sqrt(2*np.pi)) * np.exp(-(x - mean)**2/(2*sd**2))

  #logmean = np.log(mean**2/np.sqrt(sd**2 + mean**2))
  #logsd = np.sqrt(np.log(1 + sd**2/mean**2))
  #logmean = mean
  #logsd = sd
  #return yscale / (x*logsd*np.sqrt(2*np.pi)) * np.exp(-(np.log(x) - logmean)**2/(2*logsd**2))

def levy(x, params, xmode, maximum):
  location, mult = (params[0], params[1])
  scale = (xmode - location) * 3
  #mult = maximum * 2.16 * scale # 2.16 is based on levy pdf
  x = np.clip(x - location, 1, float("Inf")) # min of 1 to prevent divbyzero/log errors
  return mult * np.sqrt(scale/2*np.pi) * np.exp(-(scale/(2*(x)))) / np.power(x, 1.5)

def gamma(x, params, xmode, maximum):
  location, shape = (params[0], params[1])
  xmode -= location
  scale = xmode / (shape - 1)
  gamma_shape = integrate.quad(lambda x: x**(shape-1) * np.exp(-x), 0, float("Inf"))[0] # returns a tuple (value, error)
  mult = maximum * (gamma_shape * scale**shape) / x**(shape-1) / np.exp(-xmode/scale)
  x -= location
  return mult / (gamma_shape * scale**shape) * x**(shape-1) * np.exp(-x/scale)

# ---------  multi-guassian fit -------------

def multifit(x, y, expected_peaks):
  params, y_est = ls_fit(x, y, expected_peaks)
  peaks = [params[p] for p in xrange(0, len(params), 3)]

  print "Peaks:", peaks

  fig = plt.figure()
  ax = fig.add_subplot(111)
  ax.plot(x, y, label='Data')
  ax.plot(x, y_est, 'r-', label='Fit')
  '''
  for p in xrange(len(plsq[0])):
    ax.plot(x, pdf("normal", x, plsq[0][p:p+3]), label='Fit1')
  '''
  ax.set_xlim(0, max(x))
  ax.legend()
  plt.savefig("peak_fitting.png")

  plt.figure()
  # Sma1 digest (largest pieces):
  #x = [(8271+8612)/2,12220, 19399]
  # HndIII digest:
  x = expected_peaks
  y = peaks
  plt.scatter(x, y)
  x = x[1:]
  y = y[1:]
  a, b = np.polyfit(x, y, 1)
  x = range(20000)
  y = [a*i + b for i in x]
  plt.plot(x, y, 'r-')
  plt.savefig("peak_correlation.png")

# ---------- /multi-gaussian fit ------------



if __name__ == "__main__":
  print "usage: python pmt_analysis.py <input> <output_prefix>"

  input = sys.argv[1]
  output_prefix = sys.argv[2]

  fnames = []
  if os.path.isdir(input):
    fnames = [input + '/' + f for f in os.listdir(input) if f[-4:] == ".txt"]
  else:
    fnames=[input]

  #for fnames in [["SmaI_20140826_data/td%i.txt" % i] for i in range(1,11)]:
  #  main(fnames)

  main(fnames, output_prefix)

# ------------------------------------------------------------------------------
# Copyright (c) 2017 Jeremy Wang
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# ------------------------------------------------------------------------------

import argparse
import simulate_nibs as sim
from digest import digest
import detect_sv as detect
import matplotlib
matplotlib.use("Agg")
from matplotlib import pyplot as plt
from matplotlib.gridspec import GridSpec

def run_eval_sim(seqs, names, res, truth, trials=10, size_std = 0, digestion = 1, order_swap = 0, size_threshold=100, multiplier=False, cov=1):

  trial_results = []

  for t in xrange(trials):

    # SIMULATE DIGEST, DETECTION
    frag_sets = []
    for c in xrange(cov):
      frags, frags_pos = digest(seqs, names, res, digestion)
      if size_std > 0:
        frags = sim.size_variance(frags, size_std, multiplier=multiplier)
      if order_swap > 0:
        frags = sim.order_variance(frags, order_swap)
      frag_sets.append(frags)

    print "True fragments: %i" % len(truth)
    print "Found fragments: %i" % len(frags)

    # align each set of frags
    for f in xrange(len(frag_sets)):
      frags = frag_sets[f]
      score, path, start, density = detect.align(truth, frags, None)
      path = list(path)
      frags = list(frags)
      truth = list(truth)
      detect.adjust_path(path, frags, truth)

    truth_start, frag_start = start

    print "Path length: %i" % len(path)
    print "Path start: %i" % truth_start

    # find consensus among alignments
    # ...

    # DETECT SVs
    # (type, size, start, end[start if INS], score[None])
    svs = detect.predict_indels(path, (truth_start, frag_start), truth, frags)
    svs = [s for s in svs if s[1] >= size_threshold]
    detect.writeSVs(svs, "simulation_results/hg19.chr22/sensitivity_test.digest%.2f.size%.2f%s.order%.2f.trial%i.svs" % (digestion, size_std, ".mult" if multiplier else "",  order_swap, t))

    # STATS
    # (count, 25%, median, 75%, max)
    ref_len = sum([len(s) for k,s in seqs.iteritems()])
    sizes = [s[1] for s in svs]
    sizes.sort()
    if len(svs) > 0:
      trial_results.append((len(sizes), sizes[len(sizes)/4], sizes[len(sizes)/2], sizes[len(sizes)*3/4], sizes[-1]))
    else:
      trial_results.append((0, 0, 0, 0, 0))

  return [float(sum(t[i] for t in trial_results))/trials for i in xrange(len(trial_results[0]))]

def plot_data(data, xl, yl, yr, out):
  fig = plt.figure()
  gs = GridSpec(1,1,bottom=0.18,left=0.18,right=0.82)

  lines = []

  axL = fig.add_subplot(gs[0,0])
  for i in xrange(1, len(data[0][1])):
    lines.append(axL.plot([d[0] for d in data], [d[1][i] for d in data])[0])

  axR = axL.twinx()
  lines.append(axR.plot([d[0] for d in data], [d[1][0] for d in data], 'k')[0])

  axL.set_xlabel(xl)
  axL.set_ylabel(yl)
  axR.set_ylabel(yr)

  names = ["1st quartile", "Median", "3rd quartile", "Maximum", "# SVs detected"]
  lgd = plt.legend(lines, names, loc="center left", bbox_to_anchor=(1.13,0.5))

  plt.savefig(out, bbox_extra_artists=(lgd,), bbox_inches="tight")
  plt.clf()

if __name__ == "__main__":
  parser = argparse.ArgumentParser("Run a bunch of sensitivity tests on SV detection (simulated data)")
  parser.add_argument("fasta", help="Genome FASTA")
  parser.add_argument("REs", help="Restriction enzymes, comma separated")
  parser.add_argument("error_type", help="{digestion, size, order}")
  parser.add_argument("--start", help="Start value of error range", type=int, default=0)
  parser.add_argument("--end", help="End value (inclusive)", type=int, default=100)
  parser.add_argument("--cov", help="Coverage (to average)", type=int, default=1)
  parser.add_argument("--chrom", help="Comma-delimited list of chromosomes to use")
  args = parser.parse_args()

  chroms = args.chrom.split(',')

  seqs, names = sim.read_genome(args.fasta)
  names = [n for n in names if (chroms is None or n in chroms)]
  seqs = {n:seqs[n] for n in names}
  print "Sequences:", [(n, len(seqs[n])) for n in names]

  res = args.REs.split(',')
  print "Restriction enzyme(s) to use:", res

  num_trials = 10

  truth, truth_pos = sim.digest(seqs, names, res, 1)

  if args.error_type == "digestion":
    print "Digestion"
    data = []
    for digestion in xrange(args.start, args.end+1):
      print "Digestion %", digestion
      data.append((digestion, run_eval_sim(seqs, names, res, truth, trials=num_trials, digestion = digestion/100.0)))
    print data
    plot_data(data, "% digestion", "Size of SV (bp)", "Number of SVs detected", "digestion.png")
    plot_data(data[-20:], "% digestion", "Size of SV (bp)", "Number of SVs detected", "digestion.20.png")

  elif args.error_type == "size":
    print "Size"
    data = []
    for size_std in xrange(args.start, args.end+1):
      print "size RSD(%)", size_std
      data.append((size_std, run_eval_sim(seqs, names, res, truth, trials=num_trials, size_std = size_std/10.0, multiplier=True, cov=args.cov)))
    print data
    plot_data(data, "% variance in size", "Size of SV (bp)", "Number of SVs detected", "size.png")
    plot_data(data[:20], "% variance in size", "Size of SV (bp)", "Number of SVs detected", "size.20.png")

  elif args.error_type == "order":
    print "Order"
    data = []
    for order in xrange(args.start, args.end+1):
      print "Swap %", order
      data.append((order, run_eval_sim(seqs, names, res, truth, trials=num_trials, order_swap = order/100.0)))
    print data
    plot_data(data, "% variance in order", "Size of SV (bp)", "Number of SVs detected", "order.png")
    plot_data(data[:20], "% variance in order", "Size of SV (bp)", "Number of SVs detected", "order.20.png")

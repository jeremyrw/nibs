# ------------------------------------------------------------------------------
# Copyright (c) 2017 Jeremy Wang
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# ------------------------------------------------------------------------------

import random
import sys
import argparse
import numpy
import matplotlib
matplotlib.use("Agg")
from matplotlib import pyplot as plt
import fasta
import entropy

import digest_c

'''
A Adenine A
C Cytosine    C
G Guanine     G
T Thymine       T
U Uracil        U
W Weak  A     T
S Strong    C G
M aMino A C
K Keto      G T
R puRine  A   G
Y pYrimidine    C   T
B not A (B comes after A)   C G T
D not C (D comes after C) A   G T
H not G (H comes after G) A C   T
V not T (V comes after T and U) A C G
N or -  aNy base (not a gap)  A C G T
'''

alleles = {
  'A': ['A'],
  'C': ['C'],
  'G': ['G'],
  'T': ['T'],
  'U': ['U'],
  'W': ['A', 'T'],
  'S': ['C', 'G'],
  'M': ['A', 'C'],
  'K': ['G', 'T'],
  'R': ['A', 'G'],
  'Y': ['C', 'T'],
  'B': ['C', 'G', 'T'],
  'D': ['A', 'G', 'T'],
  'H': ['A', 'C', 'T'],
  'V': ['A', 'C', 'G'],
  'N': ['A', 'C', 'G', 'T']
}

complement = {
  'A': ['T'],
  'C': ['G'],
  'G': ['C'],
  'T': ['A'],
  'U': ['A'],
  'W': ['A', 'T'],
  'S': ['C', 'G'],
  'M': ['G', 'T'],
  'K': ['A', 'C'],
  'R': ['T', 'C'],
  'Y': ['G', 'A'],
  'B': ['G', 'C', 'A'],
  'D': ['T', 'C', 'A'],
  'H': ['T', 'G', 'A'],
  'V': ['T', 'G', 'C'],
  'N': ['T', 'G', 'C', 'A']
}


def digest(seqs, names, res, rate, nlimit=1000):

  enzymes = [line.strip().split(',') for line in open("data/re.csv").read().strip().split('\n')]
  found = [False for r in res]
  motifs = [None for r in res]
  for e in xrange(len(enzymes)):
    if enzymes[e][0] in res:
      if len(enzymes[e]) > 1:
        if '/' in enzymes[e][1]:
          enzymes[e][1] = ''.join(enzymes[e][1].split('/'))
        while '(' in enzymes[e][1]:
          enzymes[e][1] = enzymes[e][1][:enzymes[e][1].rindex('(')] + enzymes[e][1][enzymes[e][1].rindex(')')+1:]
      else:
        raise Exception("Restriction enzyme '%s' has no motif" % enzymes[e])
      print enzymes[e]
      motifs[res.index(enzymes[e][0])] = enzymes[e][1]
      for a in enzymes[e][1]:
        if not a in 'ACGT':
          raise Exception("Ambiguous motif for", enzymes[e])
      found[res.index(enzymes[e][0])] = True
      if not False in found:
        break
  else:
    for i in xrange(len(found)):
      if not found[i]:
        raise Exception("Restriction enzyme '%s' not found." % res[i])

  motif_complements = [''.join({'A':'T', 'T':'A', 'C':'G', 'G':'C'}[a] for a in m[::-1]) for m in motifs]

  frags = []
  positions = []
  for name in names:
    seq = seqs[name]

    result = digest_c.digest(seq, motifs, rate, nlimit)
    if result is None:
      return None
    sizes, locs = result
    frags.extend(sizes)
    positions.extend([(name, locs[l], locs[l]+sizes[l]) for l in xrange(len(locs))])
    '''
    start_pos = 0
    # skip Ns at the beginning of chromosomes
    while start_pos < len(seq) and  seq[start_pos] == 'N':
      start_pos += 1
    last = start_pos

    if start_pos >= len(seq):
      continue

    nstart = None
    for i in xrange(start_pos, len(seq)):

      # make implicit cuts where there are many consecutive Ns
      if seq[i] == 'N':
        if nstart is None:
          nstart = i
      else:
        if nstart is not None and i - nstart >= nlimit:
          if nstart - last > 0:
            frags.append(nstart - last)
            positions.append((name, last, nstart))
          last = i
        nstart = None

      # specific cutters only
      for m in motifs:
        if i + len(m) <= len(seq) and seq[i:i+len(m)] == m and random.random() < rate and i - last > 0:
          frags.append(i - last)
          positions.append((name, last, i))
          last = i

      # this is required for ambiguous cutters, none we are interested in require it
      """
      for a in xrange(len(e[1])):
        if seq[i+a] not in alleles[e[1][a]]:
          break
      else:
        frags.append(i-last)
        positions.append((name, last, i))
        last = i
      """

      # RC
      if last < i: # do not assess RC if the forward strand matches - most motifs are RC symmetrical
        for m in motif_complements:
          if i + len(m) <= len(seq) and seq[i:i+len(m)] == m and random.random() < rate and i - last > 0: # look for reverse complement
            frags.append(i - last)
            positions.append((name, last, i))
            last = i

    # last fragment
    if (nstart is None or i - nstart < nlimit) and i - last > 0:
      frags.append(i - last)
      positions.append((name, last, i))
    elif nstart - last > 0:
      frags.append(nstart - last)
      positions.append((name, last, nstart))
    '''

  return frags, positions


def main(fa, res, output):

  seq_name = fa[fa.rindex('/')+1 if '/' in fa else 0: fa.rindex('.') if '.' in fa else len(fa)]

  # read fasta file
  print "Reading '%s'" % fa
  seqs, names = fasta.read_fasta(fa)

  # one at a time and sort by entropy
  entropies = []
  for re in res:
    try:
      frags, positions = digest(seqs, names, [re], 1.0)
    except Exception as e:
      print e
      continue
    a = numpy.array(frags)
    entropies.append((re, entropy.shannon(a), a))
    if output is not None:
      fout = open(output + "." + re + ".frg", 'w')
      fout.write('\n'.join(str(f) for f in a))
      fout.close()
  entropies.sort(key = lambda a: -a[1])

  for e in entropies:
    small = e[2][e[2] < 1000].size
    print e[0], e[1], e[2].size, small, float(small)/e[2].size*100


  # digest them all at once
  '''
  frags, positions = digest(seqs, names, res, 1.0)
  a = numpy.array(frags)
  if output is not None:
    fout = open(output, 'w')
    fout.write('\n'.join(str(f) for f in a))
    fout.close()
  print "Shannon entropy: %.4f" % entropy.shannon(a)
  a = numpy.sort(a)
  print "Top 100:"
  print list(a[-100:])
  print "%i fragments" % a.size
  print "Mean: %.2f" % numpy.average(a)
  print "Std: %.2f" % numpy.std(a)
  quartiles = numpy.percentile(a, [0,25,50,75,100])
  print "Min: %i" % quartiles[0]
  print "1st quartile: %i" % quartiles[1]
  print "Median: %i" % quartiles[2]
  print "3rd quartile: %i" % quartiles[3]
  print "Max: %i" % quartiles[4]

  # plot only lower 99%
  hist, bin_edges = numpy.histogram(a[:a.size*0.99], bins=100)
  binsize = bin_edges[1] - bin_edges[0]
  plt.bar([bin_edges[i-1] + binsize*0.15 for i in xrange(1, len(bin_edges))], hist, binsize*0.7)
  plt.xlabel("Fragment size")
  plt.ylabel("Occurrences")
  plt.title("%s %s digestion fragment size histogram" % (seq_name, ','.join(res)))
  plt.savefig("%s.%s.digestion.png" % (seq_name, '-'.join(res)))
  '''

if __name__ == "__main__":
  parser = argparse.ArgumentParser("Digest a FASTA file with the given restriction enzyme")
  parser.add_argument("fasta", help="FASTA file")
  parser.add_argument("re", help="Restriction enzyme (or several, separated by commas)")
  parser.add_argument("--output", help="Output text file w/fragment sizes")
  args = parser.parse_args()
  main(args.fasta, args.re.split(','), args.output)


# ------------------------------------------------------------------------------
# Copyright (c) 2017 Jeremy Wang
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# ------------------------------------------------------------------------------


from string import maketrans
COMPL = maketrans("ATGC","TACG")

def rc(seq):
  return seq.translate(COMPL)[::-1]

def read_fasta(fasta):
  data = open(fasta).read().strip().split('\n')
  reads = {}
  names = []
  name = None
  seq = ""
  for d in data:
    if len(d) == 0:
      continue
    if d[0] == '>':
      if name is not None:
        reads[name] = seq
      name = d[1:]
      #if ' ' in name:
      #  name = name[:name.index(' ')]
      names.append(name)
      seq = ""
    else:
      seq += d
  reads[name] = seq
  return reads, names

def write_fasta(fname, reads, names=None):
  fout = open(fname, 'w')
  if names is None:
    names = reads.keys()
  for r in xrange(len(names)):
    if r > 0:
      fout.write('\n')
    fout.write(">%s" % names[r])
    seq = reads[names[r]]
    fout.write("\n%s" % '\n'.join([seq[i:min(len(seq),i+100)] for i in xrange(0, len(seq), 100)]))
  fout.close()


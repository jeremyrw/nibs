Tools for dynamic time warping (DTW) alignment in C
===================================================

C API
-----

See test.c:

    gcc test.c -o test
    ./test


Python API
----------

    python setup.py install

    import numpy
    import dtw_c
    query = numpy.array([0.0, 1.0, 2.0, 3.0, 4.0], dtype='f4')
    target = numpy.array([9.0, 8.0, 1.0, 0.1, 1.5, 2.0, 2.8, 4.1, 10.0, 1.0, 4.0], dtype='f4')
    res = dtw_c.align(query, target, ins_score=-2, del_score=-2)
    print "Score:", res[0]
    print "Query range: %i - %i" % (res[1], res[2])
    print "Target range: %i - %i" % (res[3], res[4])
    print "Path:", path
    print "0: Match"
    print "1: Insertion"
    print "2: Deletion"

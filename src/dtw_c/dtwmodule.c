/* ------------------------------------------------------------------------------
# Copyright (c) 2017 Jeremy Wang
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# ------------------------------------------------------------------------------*/

#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include "dtw.h"
#include "dtw.c"
#include <Python.h>
#include <numpy/arrayobject.h>

static PyObject* dtw_wrapper(PyObject * self, PyObject * args) {

  // these are required for some numpy array functions, else segfault
  Py_Initialize();
  import_array();

  // automatic parsing isn't working for some reason
  /*
  if (!PyArg_ParseTuple(args, "OOii", &query_array, &target_array, &ins_score, &del_score)) {
    printf("Unable to parse arguments\n");
    Py_RETURN_NONE;
  }
  */

  /*
  printf("Parsing arguments\n");
  size_t nargs = PyTuple_Size(&args[0]);
  printf("args tuple contains %d items.\n", nargs);
  int a;
  for(a = 0; a < nargs; a++) {
    printf("arg %d: %s\n", a, PyString_AsString(PyObject_Str(PyTuple_GetItem(&args[0], a))));
  }
  */

  // query and target array data and lengths
  PyArrayObject *target_array = (PyArrayObject*)PyTuple_GetItem(&args[0], 0);

  // check array type - this is important
  PyArray_Descr *d = PyArray_DESCR(target_array);
  if((*d).kind != 'f' || (*d).elsize != 4) {
    printf("Target array dtype is %c%d, should be f4\n", (*d).kind, (*d).elsize);
    Py_RETURN_NONE;
  }

  float *target = (float*)PyArray_DATA(target_array);
  uint32_t tlen = PyArray_DIMS(target_array)[0];

  PyArrayObject *query_array = (PyArrayObject*)PyTuple_GetItem(&args[0], 1);

  // check array type - this is important
  d = PyArray_DESCR(query_array);
  if((*d).kind != 'f' || (*d).elsize != 4) {
    printf("Query array dtype is %c%d, should be f4\n", (*d).kind, (*d).elsize);
    Py_RETURN_NONE;
  }

  float *query = (float*)PyArray_DATA(query_array);
  uint32_t qlen = PyArray_DIMS(query_array)[0];

  int8_t ins_score = PyInt_AsLong(PyTuple_GetItem(&args[0], 2));
  int8_t del_score = PyInt_AsLong(PyTuple_GetItem(&args[0], 3));
  float neutral_deviation = PyFloat_AsDouble(PyTuple_GetItem(&args[0], 4));

  //printf("qlen %d, tlen %d, ins_score %d, del_score %d\n", qlen, tlen, ins_score, del_score);

  pathvec path;
  kv_init(path);
  result res = align(query, target, qlen, tlen, &path, ins_score, del_score, neutral_deviation);

  if(res.failed) { // could not align, probably because qlen or tlen was 0
    printf("Alignment failed for some reason\n");
    Py_RETURN_NONE;
  }

  int path_len = kv_size(path);

  // ---- python list (broken) ----
  /*
  PyObject* pypath = PyList_New(path_len);
  int i;
  for(i = 0; i < path_len; i++) {
    PyList_SetItem(pypath, i, (PyObject*)PyInt_FromLong((uint8_t)kv_pop(path)));
  }
  kv_destroy(path);
  */

  // ---- numpy uint8 array ----
  int nd = 1;
  npy_intp dims[1] = {path_len};
  PyArrayObject *pypath = PyArray_SimpleNewFromData(1, dims, NPY_UINT8, (void*)path.a);

  PyObject* ret = PyList_New(6);
  PyList_SetItem(ret, 0, (PyObject*)PyFloat_FromDouble(res.score));
  PyList_SetItem(ret, 1, (PyObject*)PyInt_FromLong(res.qstart));
  PyList_SetItem(ret, 2, (PyObject*)PyInt_FromLong(res.qend));
  PyList_SetItem(ret, 3, (PyObject*)PyInt_FromLong(res.tstart));
  PyList_SetItem(ret, 4, (PyObject*)PyInt_FromLong(res.tend));
  PyList_SetItem(ret, 5, (PyObject*)pypath);

  return ret;
}

static PyMethodDef Methods[] = {
  { "align", dtw_wrapper, METH_VARARGS, "DTWAlign" },
  { NULL, NULL, 0, NULL }
};

DL_EXPORT(void) initnibs_dtw(void) {
  Py_InitModule("nibs_dtw", Methods);
}

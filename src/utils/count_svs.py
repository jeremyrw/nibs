# ------------------------------------------------------------------------------
# Copyright (c) 2017 Jeremy Wang
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# ------------------------------------------------------------------------------

import sys
import numpy

num_trials = 10

fnames = "results/hg19.chr22/sensitivity_test.digest1.00.size%.2f.order0.00.trial%i.svs"
div = 100.0

mult_fnames = "results/hg19.chr22/sensitivity_test.digest1.00.size%.2f.mult.order0.00.trial%i.svs"
div = 10.0

cov_fnames = "results/hg19.chr22/sensitivity_test.digest1.00.size%.2f.mult.order0.00.cov%i.trial%i.svs"
div = 10.0

#print "RSDmult #svs #svs>1kb #svs>10kb 1stqtl median 3rdqtl max"
print "Coverage #svs #svs>1kb #svs>10kb 1stqtl median 3rdqtl max"
#for size in xrange(1, 100):
#for size in xrange(10,11):
for size in xrange(1, 16):
  #for cov in xrange(1,6):
  for cov in xrange(1,2):
    s = size/div
    if cov == 1:
      svs = [int(line.strip().split(',')[1]) for trial in xrange(num_trials) for line in open(mult_fnames % (s, trial)).read().strip().split('\n')[1:]]
    else:
      svs = [int(line.strip().split(',')[1]) for trial in xrange(num_trials) for line in open(cov_fnames % (s, cov, trial)).read().strip().split('\n')[1:]]
    svs.sort()
    print ' '.join(str(s) for s in [size/div, len(svs)/num_trials, len([s for s in svs if s >= 1000])/num_trials, len([s for s in svs if s >= 10000])/num_trials, svs[len(svs)/4], svs[len(svs)/2], svs[len(svs)*3/4], svs[-1]])
    #print ' '.join(str(s) for s in [cov, len(svs)/num_trials, len([s for s in svs if s >= 1000])/num_trials, len([s for s in svs if s >= 10000])/num_trials, svs[len(svs)/4], svs[len(svs)/2], svs[len(svs)*3/4], svs[-1]])

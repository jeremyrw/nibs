# ------------------------------------------------------------------------------
# Copyright (c) 2017 Jeremy Wang
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# ------------------------------------------------------------------------------


'''
Reading 'data/hg19.fasta'
['ZraI', 'GACGTC']
Top 100:
[704197, 707038, 707382, 710413, 710609, 712485, 714061, 715174, 716604, 718607, 722737, 724073, 724530, 730547, 732621, 734022, 736361, 736781, 736794, 743317, 751019, 762215, 773022, 773614, 778072, 780581, 781143, 783568, 784897, 785988, 793812, 794512, 806230, 815240, 835237, 843419, 846264, 848049, 856774, 861491, 867298, 879020, 884975, 885003, 887390, 887442, 895578, 899049, 905968, 909697, 914666, 921644, 947645, 948583, 961710, 967179, 986701, 1014147, 1039778, 1059390, 1064665, 1069237, 1082367, 1083737, 1093227, 1104038, 1113072, 1125781, 1133325, 1173447, 1178346, 1260395, 1262908, 1283839, 1465465, 1505786, 1516947, 1521370, 1659482, 3101391, 3104343, 3186189, 3215182, 3327207, 3392867, 3460836, 3544732, 3590612, 3596126, 3610456, 3708303, 4012452, 4037219, 4097778, 4174774, 4610870, 11512242, 18261292, 21289178, 30261474]
44036 fragments
Mean: 68388.73
Std: 235276.36
Min: 6
1st quartile: 9649
Median: 31620
3rd quartile: 82116
Max: 30261474
'''

stage = None
re = None
stats = None
stat_types = ["Fragments", "Mean", "Std", "1st quartile", "Median", "3rd quartile", "Max"]
print "RE," + ",".join(stat_types)
for line in open("digest_all.out"):
  if ':' in line and line[:line.index(':')] == "Sender": # first line of LSF output
    stage = None
    re = None
  elif ' ' in line and line[:line.index(' ')] == "Reading":
    stage = 0
  elif stage <= 2 and line[:2] == "['":
    if re is None:
      re = line[2:2+line[2:].index("'")]
    else:
      stage -= 1
      re += "&" + line[2:2+line[2:].index("'")]
    stats = {}
  elif 4 <= stage <= 11 and re != None:
    if ':' in line:
      key, val = line.split(':')
      stats[key] = val.strip()
    elif "fragments" in line:
      stats["Fragments"] = line[:line.index(' ')]
    if stage == 11: # end of stat lines
      print "%s," % re + ','.join(stats[key] for key in stat_types)

  if stage is not None:
    stage += 1


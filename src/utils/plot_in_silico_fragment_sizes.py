# ------------------------------------------------------------------------------
# Copyright (c) 2017 Jeremy Wang
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# ------------------------------------------------------------------------------

import matplotlib
matplotlib.use("Agg")
from matplotlib import pyplot as plt

from pylab import *

samples = ["e_coli_w3110", "s_aureus_atcc_25923", "s_thermophilus_lmg_18311", "e_coli_mg1655", "hg19.chr22", "s_cerevisiae_s288c", "t_thermophilus_hb8"]

files = []
for s in samples:
  files.append(("results/%s/%s.digest1.size0.swap0.hind3.digestion" % (s, s), "%s.HindIII" % s))
  files.append(("results/%s/%s.digest1.size0.swap0.sma1.digestion" % (s, s), "%s.SmaI" % s))

boxes = []
names = []
for f,name in files:
  sizes = [int(a) for a in open(f).read().strip().split('\n')]
  boxes.append(sizes)
  names.append(name)

fig, ax = plt.subplots()

plt.boxplot(boxes, 0, '')#, labels=names)
ax.set_ylim((0,100000))
xtickNames = plt.setp(ax, xticklabels=names)
plt.setp(xtickNames, rotation=90, fontsize=8)

plt.savefig("in_silico_digestions.png", bbox_inches="tight")

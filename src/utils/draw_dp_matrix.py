# ------------------------------------------------------------------------------
# Copyright (c) 2017 Jeremy Wang
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# ------------------------------------------------------------------------------

import matplotlib
matplotlib.use("Agg")
from matplotlib import pyplot as plt
import numpy
import Image, ImageDraw, ImageFont, ImageOps


'''
  X -->
  +----------------------
Y |   ----> DEL
  |  |
| |  |   \
| |  |    \
v |  v     \
  |  INS  n/MISMATCH
  |
  |
  |
'''

DEFAULT_MARGIN = 5

def drawMatrix(outfile, x=None, y=None, matrix=None, path=None, width=1000, height=1000):
  font = ImageFont.truetype("Anonymous.ttf", 10)

  # determine text size
  im = Image.new("RGB", (100,10), "white")
  draw = ImageDraw.Draw(im)
  letter_width, letter_height = draw.textsize("ABCDEFGHIJ", font=font)
  letter_width /= 10
  axis_margin = max([letter_width * len(str(a)) for a in x+y]) + DEFAULT_MARGIN
  print "Letter size:", letter_width, letter_height
  print "Axis margin:", axis_margin, "px"

  box_width = (width - DEFAULT_MARGIN * 2 - axis_margin) / len(x)
  box_height = (height - DEFAULT_MARGIN * 2 - axis_margin) / len(y)
  print "Box size:", box_width, "x", box_height

  if box_width < DEFAULT_MARGIN * 2 or box_height < DEFAULT_MARGIN * 2:
    print "Boxes probably too small, reduce matrix size or increase image size"

  # init image, draw matrix border
  im = Image.new("RGB", (width, height), "white")
  draw = ImageDraw.Draw(im)
  draw.rectangle((axis_margin + DEFAULT_MARGIN, axis_margin + DEFAULT_MARGIN, width - DEFAULT_MARGIN, height - DEFAULT_MARGIN), outline="#000000")

  # draw x and y labels
  for a in xrange(len(x)):
    # X
    txt = Image.new('RGB', (len(str(x[a])) * letter_width, letter_height), "white")
    d = ImageDraw.Draw(txt)
    d.text((0, 0), str(x[a]), fill="#000000")#, font=font)
    w = txt.rotate(90)
    # Y
    im.paste(w, (axis_margin + int((a + 0.5) * box_width), DEFAULT_MARGIN))
  for a in xrange(len(y)):
    draw.text((DEFAULT_MARGIN, axis_margin + int((a + 0.5) * box_height)), str(y[a]), fill="#000000")#, font=font)

  # draw matrix grid and values
  for i in xrange(len(x)):
    draw.line((axis_margin + DEFAULT_MARGIN + (i+1) * box_width, axis_margin + DEFAULT_MARGIN, axis_margin + DEFAULT_MARGIN + (i+1) * box_width, height - DEFAULT_MARGIN), fill="#000000")
  for i in xrange(len(y)):
    draw.line((axis_margin + DEFAULT_MARGIN, axis_margin + DEFAULT_MARGIN + (i+1) * box_height, width - DEFAULT_MARGIN, axis_margin + DEFAULT_MARGIN + (i+1) * box_height), fill="#000000")

  # make color map for matrix values, separately for each ROW
  cmap = plt.get_cmap("jet")
  mn = matrix.min()
  range = matrix.max() - mn
  for i in xrange(len(y)):
    #mn = matrix[i,:].min()
    #range = matrix[i,:].max() - mn
    for j in xrange(len(x)):
      w = axis_margin + DEFAULT_MARGIN + j * box_width
      h = axis_margin + DEFAULT_MARGIN + i * box_height
      color = cmap((matrix[i,j] - mn) / range) # produces RGB colors with 0-1, needs to be 0-255
      color = [int(c * 255) for c in color]
      draw.rectangle((w + 1, h + 1, w + box_width - 1, h + box_height - 1), fill=tuple(color))

  im.save(outfile, "PNG")

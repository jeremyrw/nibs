# ------------------------------------------------------------------------------
# Copyright (c) 2017 Jeremy Wang
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# ------------------------------------------------------------------------------

import numpy as np
import argparse
import matplotlib
matplotlib.use("Agg")
from matplotlib import pyplot as plt
from scipy.optimize import leastsq

def main(f, out, fit_peaks=False, log_scale=False, bin_size=0.05):
  frags = [float(line.strip()) for line in open(f)]

  print "Computing and plotting fragment size histogram."
  hist = [0 for i in xrange(int(max(frags)/bin_size) + 1)]
  for f in frags:
    hist[int(f / bin_size)] += 1
  print "Plotting fragment sizes."
  x = np.array(range(len(hist)))
  y = np.array(hist, dtype='u4')
  if fit_peaks:
    # for peak fitting, get rid of everything under 5
    y = y / np.max(y) # normalize to 0-1, this is necessary to fit properly
    # lambda HindIII digest:
    #multifit(x, y, [(a/100, 1, 2) for a in [125, 564, 2027, 2322, 4361, 6557, 9416, 23130]])
    # lambda SmaI digest:
    multifit(x, y, [(a/800, 1, 2) for a in [(8271+8612)/2, 12220, 19399]])
  if log_scale:
    for h in xrange(len(hist)):
      hist[h] = math.log(hist[h], 2) if hist[h] > 0 else 0
  fig = plt.figure()
  ax = fig.add_subplot(111)
  ax.set_ylabel("# fragments")
  ax.set_xlabel("Total fragment intensity")
  x = [i*bin_size for i in xrange(len(hist))]
  ax.plot(x, hist)
  ax.set_xlim(0, max(x))
  if log_scale:
    ax.yaxis.set_ticklabels([(2**i) for i in xrange(0,15,2)])
  plt.savefig(out)

def multifit(x, y, expected_peaks):
  params, y_est = ls_fit(x, y, expected_peaks)
  peaks = [params[p] for p in xrange(0, len(params), 3)]

  fig = plt.figure()
  ax = fig.add_subplot(111)
  ax.plot(x, y, label='Data')
  ax.plot(x, y_est, 'r-', label='Fit')
  ax.set_xlim(0, max(x))
  ax.legend()
  plt.savefig("peak_fitting.png")

  x = [e[0] for e in expected_peaks]
  y = peaks
  plt.figure()
  plt.scatter(x, y)
  x = x[1:]
  y = y[1:]
  a, b = np.polyfit(x, y, 1)
  x = range(20000)
  y = [a*i + b for i in x]
  plt.plot(x, y, 'r-')
  plt.savefig("peak_correlation.png")

def ls_fit(x, y, expected_peaks, fit="normal"):
  num_params = len(expected_peaks[0])

  # solving
  params = [param for peak in expected_peaks for param in peak] # flatten

  def res(params, y, x):
    y_fit = sum([pdf(fit, x, params[p:p+num_params]) for p in xrange(0, len(params), num_params)])
    err = y - y_fit
    return err

  plsq = leastsq(res, params, args = (y, x), maxfev=10000)
  #plsq = simple_opt(res, params, args = (y, x))

  y_est = sum([pdf(fit, x, plsq[0][p:p+num_params]) for p in xrange(0, len(plsq[0]), num_params)])

  return plsq[0], y_est

# probability density function
def pdf(fit, x, params):
  pdf = normal(x, params)
  return np.array(pdf)

def normal(x, params):
  mean, sd, mult = (params[0], params[1], params[2])
  return mult / (sd*np.sqrt(2*np.pi)) * np.exp(-(x - mean)**2/(2*sd**2))

if __name__ == "__main__":
  parser = argparse.ArgumentParser("Plot distribution of fragment sizes, real or simulated")
  parser.add_argument("frags", help="List of fragment sizes")
  parser.add_argument("out", help="Output (PNG) file")
  parser.add_argument("--fit", help="Fit peaks with set of gaussians", default=False, action="store_true")
  parser.add_argument("--bin", help="Histogram bin size", default=0.05, type=float)
  parser.add_argument("--log", help="Log-scale y axis", default=False, action="store_true")
  args = parser.parse_args()
  main(args.frags, args.out, args.fit, args.log, args.bin)
